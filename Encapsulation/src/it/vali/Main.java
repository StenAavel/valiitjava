package it.vali;

public class Main {
// Encapsulation on selleks, et meetodis ei saaks muutujaid tuksi keerata, selleks luuakse neile parameetrid
    public static void main(String[] args) {
        // int vaikeväärtus 0
        // boolean vaikeväärtus false
        // double vaikeväärtus 0.0
        // String vaikeväärtus null
        // Objektide (nt. Monitor) vaikeväärtus null
        // int[] arvud; (massiivi) vaikeväärtus null
        // float vaikeväärtus 0.0f

        Monitor firstMonitor = new Monitor();
        firstMonitor.setDiagonal(200);
        System.out.println(firstMonitor.getDiagonal());


        System.out.println(firstMonitor.getYear());

        firstMonitor.setManufacturer("Huawei");
        firstMonitor.printInfo();
        firstMonitor.setManufacturer(null);

    }
}
