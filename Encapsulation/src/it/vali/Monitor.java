package it.vali;

// Enum on tüüp, kus saab defineerida erinevaid lõplikke valikuid
// Tegelikult salvestatakse enum alati int-na (kui numbrit juurde ei kirjuta, pannakse automaatselt järjekorranumbrid
// nt BLACK = 1, WHITE = 2 jne.)
enum Color {
    BLACK,
    WHITE,
    GREY
}

enum ScreenType {
    LCD,
    TFT,
    OLED,
    AMOLED
}

public class Monitor {
    private String manufacturer;
    private double diagonal; // tollides "
    private Color color;
    private ScreenType screenType;

    public int getYear() {
        return year;
    }

    private int year = 2000;

    public double getDiagonal() {
        if(diagonal == 0) {
            System.out.println("Diagonaal on seadistamata");
        }
        return diagonal;
    }
    public void setDiagonal(double diagonal) {
        // this tähistab seda konkreetset objekti
        if(diagonal<0) {
            System.out.println("Diagonaal ei saa olla negatiivne");
        } else if(diagonal>100) {
            System.out.println("Diagonaal ei saa olla suurem kui 100\"");
        }
        else {
            this.diagonal = diagonal;
        }

    }
    // Ära luba seadistada monitori tootjaks, mille tootja on Huawei
    // Kui keegi soovib seda tootjat monitori tootjaks panna, pannakse hoopis tekst "Tootja puudub"
    // Keela ka tühja tootjanime lisamine "", null
    public String getManufacturer() {
        return manufacturer;
    }


    public void setManufacturer(String manufacturer) {

        if (manufacturer==null || manufacturer.equals("Huawei") || manufacturer.equals("")) {
            this.manufacturer = "Tootja puudub";
        }
        else {
            this.manufacturer = manufacturer;
        }
    }

    public Color getColor() {
        return color;
    }
    public void setColor(Color color) {
        this.color = color;
    }
    public ScreenType getScreenType() {
        if(screenType==null) {
            return ScreenType.LCD;
        }
        return screenType;
    }
    // Kui ekraani tüüp on seadistamata (null), siis tagasta tüübiks LCD
    public void setScreenType(ScreenType screenType) {
        this.screenType = screenType;
    }


    void printInfo() {
        System.out.println();
        System.out.println("Monitori info:");
        System.out.printf("Tootja: %s%n", manufacturer);
        System.out.printf("Diagonaal: %.1f%n", diagonal);
        System.out.printf("Värv: %s%n", color);
        System.out.printf("Ekraani tüüp: %s%n", getScreenType());
        System.out.printf("Aasta: %d%n", year);
        System.out.println();
    }
    // Tee meetod, mis tagastab ekraani diagonaali sentimeetrites

    double converseToCm() {
        return diagonal * 2.54;

    }

}
