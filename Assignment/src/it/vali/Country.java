package it.vali;

import java.util.List;

public class Country {
    private int population;
    private String Name;
    private List languages;

    public List getLanguages() {
        return languages;
    }

    public void setLanguages(List languages) {
        this.languages = languages;
    }

    @Override
    public String toString() {
        return "Riigi " + Name + " populatsioon on " + population + " ja seal räägitakse keeli " + languages;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public Country(String name, int population) {
        Name = name;
        this.population = population;
    }


}
