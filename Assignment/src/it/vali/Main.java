package it.vali;

import java.util.ArrayList;
import java.util.List;



public class Main {



    public static void main(String[] args) {
        // 1
        double pi = (float) Math.PI;
        System.out.printf("Pi väärtus on %.11f%n", pi);

        System.out.println();

        // 2
        if (equalNumbers(2, 3)) {
            System.out.println("Numbrid on võrdsed");
        } else {
            System.out.println("Numbrid ei ole võrdsed");
        }
        System.out.println();
        // 3 ei mõelnud välja :)

        // 4
        System.out.println(years(0));
        System.out.println(years(1));
        System.out.println(years(128));
        System.out.println(years(598));
        System.out.println(years(1624));
        System.out.println(years(1827));
        System.out.println(years(1996));
        System.out.println(years(2017));

        System.out.println();

        // 5
        Country country = new Country("Estonia", 14000000);
        country.setName("Estonia");
        List<String> languages = new ArrayList<String>();
        languages.add("Estonian");
        languages.add("Russian");
        languages.add("English");
        country.setPopulation(1400000);
        country.setLanguages(languages);

        System.out.println(country.toString());


    }

    // 2 meetod
    static boolean equalNumbers(int one, int two) {
        if (one == two) {
            return true;
        }
        return false;
    }

    // 3 ei mõelnud välja :)
//    public int[] numbers(String[] words) {
//        int[] numbers = new int[words.length];
//        int counter = 0;
//        String sentence = null;
//        String word;
//        word =  String.join(" ", words);
//        String wordCount;
//        wordCount = sentence.substring(0, words.length-1);
//
//        } return
//
//
//    }

    // 4 meetod
    public static byte years(int year) {
        if(year>2018||year<-1) {
            System.out.println(-1);
        }
        int century = year/100+1;
            return (byte)century;


    }



}