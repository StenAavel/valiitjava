package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {


//        System.out.println("Kas tahad jätkata? Jah/ei");
//        // Jätkame seni, kuni kasutaja kirjutab "ei"
//        Scanner scanner = new Scanner(System.in);
//        String answer = scanner.nextLine();
//        while(!answer.equals("ei")) {
//            System.out.println("Proovi uuesti");
//            answer = scanner.nextLine();
        Scanner scanner = new Scanner(System.in);
        String answer;
        // Iga muutuja mille me deklareerime, kehtib ainult
        // seda ümbritsevate sulgude sees

        do {
            System.out.println("Kas tahad jätkata? Jah/ei");
            answer = scanner.nextLine();
        }
        while(!answer.equals("ei"));
        // Do while tsükkel on nagu while tsükkel, ainult et kontroll tehakse
        // peale tsükli esimest kordust

        
        }
}
