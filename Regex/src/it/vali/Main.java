package it.vali;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    public static void main(String[] args) {
        System.out.println("Sisesta email");
        Scanner scanner = new Scanner(System.in);

        String email = scanner.nextLine();
        String regex = "([a-z0-9\\._]{1,50})@[a-z0-9]{1,50}\\.([a-z]{2,10})\\.?([a-z]{2,10})?";

        if (email.matches("([a-z0-9\\._]{1,50})@[a-z0-9]{1,50}\\.([a-z]{2,10})\\.?([a-z]{2,10})?")) {
            System.out.println("Email oli korrektne");
        } else {
            System.out.println("Email ei olnud korrektne");
        }
        Matcher matcher = Pattern.compile(regex).matcher(email);
        if (matcher.matches()) {
            System.out.println("Kogu email: " + matcher.group(0));
            System.out.println("Tekst vasakulpool @ märki: " + matcher.group(1));
            System.out.println("Domeeni laiend: " + matcher.group(2));
        }

        // Küsi kasutajalt isikukood ja valideeri kas see on õiges formaadis
        // ja prindi välja tema sünnipäev
        // Mõelge ise, mis piirangud isikukoodis peaks olema
        // Nt aasta peab olema reaalne aasta, kuu number, kuupäeva number (kodutöö)
        System.out.println("Sisesta isikukood");
        String id = scanner.nextLine();
        String regexTwo = "^[0-9]{11}$";
        if (id.matches("^[0-9]{11}$")) {
            System.out.println("Isikukood oli korrektne");
        } else {
            System.out.println("Isikukood ei olnud korrektne");
        }
        Matcher matcherTwo = Pattern.compile(regexTwo).matcher(id);
        if (matcher.matches()) {
            
        }
    }
}