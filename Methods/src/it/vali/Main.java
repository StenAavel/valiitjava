package it.vali;

public class Main {
    // Meetodid on mingid koodi osad, mis grupeerivad mingit teatud
    // kindlat funktsionaalsust

    // Kui koodis on korduvaid koodi osasid, võiks mõelda,
    // et äkki peaks nende kohta tegema eraldi meetodi
    public static void main(String[] args) {
        printHello();
        printHello(3);
        printHello(2);
        textPrint("Kuidas läheb?");
        printTimes("Mis toimub?", 3);
        whichLetters("Millal?", 2, true);
    }

    // Lisame meetodi, mis prindib ekraanile Hello
    private static void printHello() {
        System.out.println("Hello");
    }

    // Lisame meetodi, mis prindib Hello etteantud arv kordi
    static void printHello(int howManyTimes) {
        for (int i = 0; i < howManyTimes; i++) {
            System.out.println("Hello");
        }

    }

    // Lisage meetod, mis prindib ette antud teksti välja

    static void textPrint(String text) {
        System.out.println(text);
    }

    // Lisage meetod, mis prindib ette antud teksti välja ette antud arv kordi
    static void printTimes(String newText, int timeCount) {
        for (int i = 0; i < timeCount; i++) {
            System.out.println(newText);
        }
    }

    // Method OVERLOADING - meil on mitu meetodit sama nimega, aga erineva parameetrite kombinatsiooniga
    // <- Meetodi ülelaadimine (küsitakse tihti tööintervjuudel, mis see on)
    // Koosta eelmisega muidu sama meetod, kus lisaks saab öelda, kas tahame teha kõik tähed enne suurteks või mitte
    // kui meetod ei lase parameetreid kasutada, siis panna need teise järjekorda või muuta parameetrite nimesid
    static void whichLetters(String text, int howManyTimes, boolean toUpperCase) {
        for (int i = 0; i < howManyTimes; i++) {
            if (toUpperCase) {
                System.out.println(text.toUpperCase());

            } else {
                System.out.println(text);
            }
        }

    }


}