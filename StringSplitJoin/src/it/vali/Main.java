package it.vali;

public class Main {

    public static void main(String[] args) {

//        String word = "kala";
//        int length = word.length();
//        System.out.println(length);

        String[] words = new String[]{"Põdral", "maja", "metsa", "sees" };
        for (int i = 0; i <words.length ; i++) {
            System.out.println(words[i]);

        }
        System.out.println();
        // Prindi kõik sõnad massiivist, mis algavad m-tähega


        for (int i = 0; i <words.length ; i++) {
            String firstLetter = words[i].substring(0, 1);

            if(firstLetter.toLowerCase().equals("m")) {
                System.out.println(words[i]);
            }

        }
        System.out.println();
        // Prindi kõik a-tähega lõppevad sõnad (kodutöö)

        for (int i = 0; i < words.length; i++) {
            String lastLetter = words[i].substring(words[i].length()-1);


            if (lastLetter.equals("a")) {
                System.out.println(words[i]);
            }

        }
        System.out.println();

        // Loe üle kõik sõnad, mis sisaldavad a-tähte ja prindi see sõnade arv välja (kodutöö)
        System.out.println("a sõnad: ");
        String sona;
        int loe = 0;
        for (int i = 0; i < words.length ; i++) {
            sona = words[i];
            if(sona.contains("a")){
                System.out.println(sona);
                loe++;
            }
        }
        System.out.println(loe);

        System.out.println();




        // Prindi välja kõik sõnad, kus on 4 tähte (kodutöö)
        sona = "";
        for (int i = 0; i < words.length; i++){
            sona = words[i];
            if (sona.length() == 4){
                System.out.println(words[i]);
            }
        }




        System.out.println();

        int pikim = 0;
        // Prindi välja kõige pikem sõna (kodutöö)
        String longestWord = words[0];
        for (int i = 1; i < words.length; i++) {
            if(words[i].length() > longestWord.length()) {
                longestWord = words[i];
            }

        }
        System.out.println(longestWord);

        System.out.println();

        // Teine võimalus:

        for (int i = 0; i < words.length; i++){
            while (words[pikim].length() < words[i].length()){
                pikim ++;
                //System.out.println(pikim);
            }
        }
        System.out.println(words[pikim]);

        System.out.println();

        // Prindi välja sõna, kus esimene ja viimane täht on samad (kodutöö)

        for(int i = 0; i < words.length; i++){
            String esimene = words[i].substring(0, 1);
            String viimane = words[i].substring(words[i].length()-1);
            if(esimene.equals(viimane)){
                System.out.println(words[i]);
            }
        }







    }
}
