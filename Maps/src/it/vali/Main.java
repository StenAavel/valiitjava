package it.vali;

import java.sql.SQLOutput;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        Map<String, String> map = new HashMap<String, String>();

        // Sõnaraamat
        // key => value
        // Maja => House
        // Isa => Dad
        // Puu => Tree

        map.put("Maja", "House");
        map.put("Isa", "Dad");
        map.put("Puu", "Tree");
        map.put("Sinine", "Blue");


        for (Map.Entry<String, String> entry : map.entrySet()    ) {
            System.out.println(entry.getKey() + " " + entry.getValue());

        }


        // Oletame, et tahan teada, mis on inglise keeles Puu

        String translation = map.get("Puu");
        System.out.println(translation);

        Map<String, String> idNumberName = new HashMap<String, String>();
        idNumberName.put("3850233234553", "Lauri");
        idNumberName.put("3873353252665", "Malle");
        idNumberName.put("3873353252665", "Kalle");

        // Kui kasutada put sama key lisamisel, kirjutatakse value üle
        // Key on unikaalne


        System.out.println(idNumberName.get("3873353252665"));
        idNumberName.remove("3873353252665");
        System.out.println(idNumberName.get("3873353252665"));
        // EST => Estonia
        // Estonia => +372

        // Loe lauses üle kõik erinevad tähed
        // ning prindi välja iga tähe järel, mitu tükki teda selles lauses oli


        // Char on selline tüüp, kus saab hoida üksikut sümbolit
        char symbolA = 'a';
        char symbolB = 'b';
        char newLine = '\n';


        String sentence = "elas metsas mutionu";
        Map<Character, Integer> letterCounts = new LinkedHashMap<Character, Integer>();

        char[] characters = sentence.toCharArray();

        for (int i = 0; i < characters.length; i++) {
            if(letterCounts.containsKey(characters[i])) {

                letterCounts.put(characters[i], letterCounts.get(characters[i]) + 1);
            }else {
                letterCounts.put(characters[i], 1);
            }
        }

        // Map.Entry<Character, Integer> on klass, mis hoiab endas ühte rida map-is
        // ehk ühte key-value paari

        System.out.println(letterCounts);
        for (Map.Entry<Character, Integer> entry : letterCounts.entrySet()) {
            System.out.printf("Tähte %s esines %d korda%n", entry.getKey(), entry.getValue());
        }


        // e 2
        // l 1
        // a 2
        // s 3

        // Kui tahame, et säiliks lisamise järjekord, kastutame LinkedHashMapi
        Map<String, String> linkedMap = new LinkedHashMap<String, String>();

        // Sõnaraamat
        // key => value
        // Maja => House
        // Isa => Dad
        // Puu => Tree

        linkedMap.put("Maja", "House");
        linkedMap.put("Isa", "Dad");
        linkedMap.put("Puu", "Tree");
        linkedMap.put("Sinine", "Blue");


        for (Map.Entry<String, String> entry : linkedMap.entrySet()    ) {
            System.out.println(entry.getKey() + " " + entry.getValue());

        }


    }
}
