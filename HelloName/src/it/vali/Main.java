package it.vali;

public class Main {

    public static void main(String[] args) {
        // Deklareerime/defineerime tekstitüüpi  (String) muutuja (variable)
        // mille nimeks paneme name ja väärtuseks Sten
	String name = "Sten";
	String lastName = "Aavel";

	System.out.println("Hello " + name + " " + lastName);
    }
}
