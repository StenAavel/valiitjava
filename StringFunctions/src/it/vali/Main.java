package it.vali;

public class Main {

    public static void main(String[] args) {
        String sentence = "Elutses metsas Mutionu keset kuuski noori vanu";
        // Leia üles esimene tühik ja mis on tema indeks
        // Sümbolite indeksid tekstis algavad samamoodi indeksiga 0, nagu massiivides
        int spaceIndex = sentence.indexOf(" ");
        // indexOf tagastab -1, kui otsitavat fraasi/sümbolit ei leitud
        // ning indeksi (kust sõna algab) kui fraas leitakse
        System.out.println(spaceIndex);


        // Prindi välja kõigi tühikute indeksid lauses

        while (spaceIndex != -1) {
            System.out.println(spaceIndex);
            spaceIndex = sentence.indexOf(" ", spaceIndex + 1);
        }

        spaceIndex = sentence.lastIndexOf(" ");

        while (spaceIndex != -1) {
            System.out.println(spaceIndex);
            spaceIndex = sentence.lastIndexOf(" ", spaceIndex - 1);
        }

        spaceIndex = sentence.indexOf(" ");
        String part = sentence.substring(0, spaceIndex);
        System.out.println(part);


        // Prindi lause teine sõna


        int secondSpaceIndex = sentence.indexOf(" ", spaceIndex + 1);
        String letter = sentence.substring(spaceIndex + 1, secondSpaceIndex);
        System.out.println(letter);


        // Leia esimene K tähega algav sõna
        String firstLetter = sentence.substring(0, 1);
        String kWord = "";


        if (firstLetter.equals("k")) {
            spaceIndex = sentence.indexOf(" ");
            kWord = sentence.substring(0, spaceIndex);

        } else {
            int kindex = sentence.indexOf(" k") + 1;
            if (kindex != 0) {
                spaceIndex = sentence.indexOf(" ", kindex);
                kWord = sentence.substring(kindex, spaceIndex);
            }
        }
        if (kWord.equals("")) {
            System.out.print(kWord);
    }
        else {
            System.out.printf("Esimene k-tähega algav sõna on %s%n", kWord);
        }


            // Leia mitu sõna lauses on
        System.out.println();

        int spaceCounter = 0;
        spaceIndex = sentence.indexOf(" ");
        while (spaceIndex != -1) {
            spaceIndex = sentence.indexOf(" ", spaceIndex + 1);
            spaceCounter++;
        }
        System.out.printf("Sõnade arv lauses on %d%n", spaceCounter + 1);

        // Leia mitu k-tähega algavat sõna on lauses (kodutöö)
        System.out.println();

        int kCounter = 0;
        sentence = sentence.toLowerCase();
        int kIndex = sentence.indexOf(" k");
        while (kIndex != -1) {
            kIndex = sentence.indexOf(" k", kIndex + 1);
            kCounter++;
        }
        if(sentence.substring(0, 1).equals("k")) {
            kCounter++;
        }

        System.out.printf("K-tähega algavate sõnade arv lauses on %d%n", kCounter);








    }
}