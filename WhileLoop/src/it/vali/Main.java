package it.vali;

import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class Main {

    public static void main(String[] args) {

//        while(true) {
//            System.out.println("Tere");
//        }
        // Lõputu tsükkel while loopiga

        // Programm mõtleb numbri
        // Programm küsib kasutajalt, et ta arvaks numbri ära
        // Seni kuni kasutaja arvab valesti, ütleb et proovi uuesti
        Scanner scanner = new Scanner(System.in);

        Random random = new Random();
        // random.nextInt(5) genereerib tgt numbrid 0-4, liites ühe juurde saab nullist viieni
        do {
            int number = random.nextInt(5)+1;
            // 80-100
            // int number = random.nextInt(20)+80;


            int randomNum = ThreadLocalRandom.current().nextInt(1, 6);
            // Teine võimalus sama asja teha


            System.out.println("Mõtlesin välja numbri. Arva number nullist viieni ära.");
            int enteredNumber = Integer.parseInt(scanner.nextLine());
            while(enteredNumber!=number) {
                System.out.println("Proovi uuesti");
                enteredNumber = Integer.parseInt(scanner.nextLine());
            }
            System.out.println("Tubli! Arvasid numbri ära!");
            System.out.println("Kas soovid veel mängida?");
        } while (!scanner.nextLine().equals("ei"));


        // While tsükkel on tsükkel, kus korduste arv ei ole teada






    }
}
