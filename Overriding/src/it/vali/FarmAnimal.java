package it.vali;

public class FarmAnimal extends DomesticAnimal {
    private int averageHerdSize;

    public int getAverageHerdSize() {
        return averageHerdSize;
    }

    public void setAverageHerdSize(int averageHerdSize) {
        this.averageHerdSize = averageHerdSize;
    }

    public void home() {
        System.out.println("Elan farmis");
    }
}
