package it.vali;

public class Main {

    public static void main(String[] args) {
	// Method Overriding ehk meetodi ülekirjutamine
    // tähendab seda, et kuskil klassis, millest antud klass pärineb, oleva meetodi sisu
    // kirjutatakse pärinevas klassis üle

        Dog tax = new Dog();
        tax.setName("Muki");
        tax.setAge(0);
        tax.setBreed("");
        tax.setWeight(3.22);

        Cat persian = new Cat();
        persian.setName("Liisu");
        persian.setAge(3);
        persian.setBreed("");
        persian.setWeight(3.11);

        tax.eat();
        persian.eat();

        System.out.println();


        persian.printInfo();

        System.out.println();

        Pet pet = new Pet();
        pet.printInfo();

        System.out.println(tax.getAge());

        System.out.println();

        tax.printInfo();

        System.out.println();

        CarnivoreAnimal lion = new CarnivoreAnimal();
        lion.setBreed("simba");
        lion.getBreed();
        lion.printInfo();

        // Super otsib pärislusahelast lähimat meetodit, kus märgitud parameeter on olemas

        // Kirjuta koera getAge üle nii, et kui koera vanus on 0, siis näitaks ikka 1
        // Metsloomadel võiks printInfo kirjutada, et Tõug: metsloomadel pole tõugu



    }
}
