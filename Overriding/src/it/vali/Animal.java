package it.vali;

public class Animal {
    private String breed;
    private int age;
    private double weight;



    public String getBreed() {
        if(breed==null) {
            System.out.println("Tõug puudub");
        }
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void eat() {
        System.out.println("Söön toitu");
    }

    public void printInfo() {
        System.out.printf("Tõug %s%n", getBreed());
        System.out.printf("Vanus %d%n", age);
        System.out.printf("Kaal %.2f%n", weight);



    }

}
