package it.vali;

public class DomesticAnimal extends Animal {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void friendly() {
        System.out.println("Olen inimsõbralik");
    }
}
