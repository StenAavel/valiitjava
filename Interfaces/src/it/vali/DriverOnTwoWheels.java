package it.vali;

public interface DriverOnTwoWheels extends Driver {
    void driveOnRearWheel();
}
