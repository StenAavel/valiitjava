package it.vali;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        Flyer bird = new Bird();
        Flyer plane = new Plane(8000);
        Driver car = new Car(300);
        Driver biggerPlane = new Plane(9000);
        Driver motorcycle = new Motorcycle(150);

        bird.fly();
        System.out.println();
        plane.fly();
        System.out.println();

        List<Flyer> flyers = new ArrayList<Flyer>();
        flyers.add(bird);
        flyers.add(plane);

        Plane boeing = new Plane(10000);
        Bird pigeon = new Bird();

        flyers.add(boeing);
        flyers.add(pigeon);

        System.out.println();

        for (Flyer flyer: flyers) {
            flyer.fly();
            System.out.println();
        }

        System.out.println();

        car.drive();
        car.stopDriving(200);
        biggerPlane.stopDriving(9000);

        List<Driver> drivers = new ArrayList<Driver>();
        drivers.add(car);
        drivers.add(biggerPlane);
        System.out.println();
        System.out.println(car.getMaxDistance());

        for (Driver driver: drivers) {
            driver.stopDriving(200);
            System.out.println();
        }

        Car carTwo = new Car(12);
        carTwo.setMake("Audi");
        System.out.println(carTwo);

        // Lisa interface Driver, class Car. Mõtle kas lennuk ja auto võiks mõlemad kasutada Driver-interface-i? (kodutöö)
        // Driver interface võiks sisaldada 3 meetodi kirjeldust:
        // int getMaxDistance()
        // void drive()
        // void stopDriving(int afterDistance)
        // Pane auto ja lennuk mõlemad kasutama seda interface-i

        // Lisa ka mootorratas (lisaülesanne koju)
    }
}
