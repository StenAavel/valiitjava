package it.vali;
// Plane pärineb klassist Vehicle ja implementeerib liidest(interface) Flyer
public class Plane extends Vehicle implements Driver, Flyer {
    private int maxDistance = 9000;

    public Plane(int maxDistance) {
        this.maxDistance = maxDistance;
    }

    @Override
    public int getMaxDistance() {
        return maxDistance;
    }

    public void fly() {
        doChecklist();
        startEngine();
        System.out.println("Lennuk lendab");
    }

    private void doChecklist() {
        System.out.println("Täidetakse checklist");
    }

    private void startEngine() {
        System.out.println("Mootor käivitus");
    }

    @Override
    public void drive() {
        doChecklist();
        System.out.println("Lennuk alustas sõitmist maandumisrajal");
    }

    @Override
    public void stopDriving(int afterDistance) {
            System.out.printf("Lennuk lõpetas sõitmise peale %d km%n", afterDistance);

    }


}
