package it.vali;

public class Motorcycle extends Vehicle implements Driver, DriverOnTwoWheels {
    private int maxDistance = 300;

    public Motorcycle(int maxDistance) {
        this.maxDistance = maxDistance;
    }

    @Override
    public int getMaxDistance() {
        return maxDistance;
    }

    @Override
    public void drive() {
        System.out.println("Mootorratas alustas sõitmist");
    }

    @Override
    public void stopDriving(int afterDistance) {
        System.out.printf("Mootorratas lõpetas sõitmise peale %d km%n", afterDistance);

    }

    @Override
    public void driveOnRearWheel() {

    }
}
