package it.vali;

public class Car extends Vehicle implements Driver {
    private int maxDistance = 300;
    private String make;

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public Car(int maxDistance) {
        this.maxDistance = maxDistance;
    }

    @Override
    public int getMaxDistance() {
        return maxDistance;
    }

    @Override
    public void drive() {
        System.out.println("Auto alustas sõitmist");
    }

    @Override
    public void stopDriving(int afterDistance) {
            System.out.printf("Auto lõpetas sõitmise peale %d km%n", afterDistance);

    }

    @Override
    public String toString() {
        return "Auto andmed: " + make + " " + maxDistance;
    }
}
