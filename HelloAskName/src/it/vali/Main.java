
package it.vali;
// import tähendab, et antud klassile Main lisatakse ligipääs java class library paketile
// java.util paiknevale klassile Scanner
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // Loome Scanner tüübist objekti nimega scanner, mille kaudu saab kasutaja sisendit lugeda

        Scanner scanner = new Scanner(System.in);

        System.out.println("Tere, mis su nimi on?");
        String name = scanner.nextLine();

      System.out.println("Mis su lemmik värv on?");
      String color = scanner.nextLine();

      System.out.println("Mis autoga sa sõidad?");
      String car = scanner.nextLine();

      System.out.println("Tere " + name + " tore värv on " + color + " ja auto " + car + " on ka äge.");

      System.out.printf("Tere %s tore värv on %s ja auto %s on ka äge.\n",
                name, color, car);

      StringBuilder builder = new StringBuilder();
      builder.append("Tere ");
      builder.append(name);
      builder.append(" tore värv on ");
      builder.append(color);
      builder.append(" ja auto ");
      builder.append(car);
      builder.append(" on ka äge.");

      String fullText = builder.toString();

      System.out.println(fullText);

      // Nii saab lihtsalt teha stringi, kui ei taha välja printida, vaid lihtsalt alles jätta
        // Nii System.out.printf kui ka String.format kasutavad enda siseselt StringBuilderit
      String text = String.format("Tere %s tore värv on %s ja auto %s on ka äge.\n",
              name, color, car);

      System.out.println(text);


    }
}
