package it.vali;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;


public class Main {
    static String pin;
    public static int newTransaction;
    static String transactions;
    public static String line;

    public static void main(String[] args) {
        pin = loadPin();
        savePin(pin);
        transaction();
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();


    }

    private static void transaction(){
        Scanner scanner = new Scanner(System.in);
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();


        int choice;
        int balance = loadBalance();
        String newpin;


        if(validatePin()==true) {



        System.out.println("Vali tehing");
        System.out.println("1 Raha väljavõtmine");
        System.out.println("2 Raha sissemakse");
        System.out.println("3 Kontojääk");
        System.out.println("4 Konto väljavõte");
        System.out.println("5 Pin koodi muutmine");

        choice = scanner.nextInt();
        newpin = scanner.nextLine();

        switch(choice) {
            case 1:
                int amount;
                System.out.println("Palun sisesta summa: ");
                amount = scanner.nextInt();
                if (amount > balance) {
                    System.out.println("Kontol pole piisavalt vahendeid\n\n");
                    newTransaction();
                }
                else if (!(amount % 5 == 0)) {
                    System.out.println("Saadaval on vaid kupüürid 5, 10, 20, 50. Palun valige teine summa.");
                    newTransaction();
                }

                else {
                    int newBalance = balance - amount;
                    saveBalance(newBalance);
                    System.out.printf("Raha väljastatud. Teie uus kontojääk on %d%n", newBalance);
                    loadTransactions();
                    saveTransactions(transactions=transactions + "Väljastatud " + amount + " EUR " + date + System.lineSeparator());
                    newTransaction();
                }
                break;

            case 2:
                int deposit;
                System.out.println("Palun sisestage summa: ");
                deposit = scanner.nextInt();
                int newBalance = balance + deposit;
                saveBalance(newBalance);
                System.out.printf("Raha sisestatud. Teie uus kontojääk on %d%n", newBalance);
                loadTransactions();
                saveTransactions(transactions=transactions + "Sisestatud " + deposit + " EUR " + date + System.lineSeparator());
                newTransaction();
                break;

            case 3:
                System.out.printf("Teie kontojääk on %d%n", balance);
                newTransaction();
                break;

            case 4:
                transactions = loadTransactions();
                System.out.println(transactions);
                newTransaction();
                break;

            case 5:
                System.out.println("Uus pin: ");
                newpin = scanner.nextLine();
                System.out.println("Pin-kood muudetud.");
                savePin(newpin);
                newTransaction();
                break;


            default:
                System.out.println("Esines viga. Palun valige uuesti.");
                newTransaction();
                break;
        }
        } else {
            System.out.println("Pin-kood oli vale. Konto ajutiselt blokeeritud.");
        }

    }

    public static void newTransaction(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Kas soovite uut tehingut? 1 Jah, 2 Ei");
        newTransaction = scanner.nextInt();
        if(newTransaction == 1){
            transaction();
        } else if(newTransaction == 2){
            System.out.println("Tänan meie teenuse kasutamise eest. Head päeva!");
        } else {
            System.out.println("Esines viga. Palun proovige uuesti.");
            newTransaction();
        }
    }

    static String currentDateTimeToString() {
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss EEEEE");

        Date date = new Date();
        return dateFormat.format(date);
    }


    static void saveTransactions(String transactions) {
        try {
            FileWriter fileWriter = new FileWriter ("transactions.txt",true);
            fileWriter.append(transactions + System.lineSeparator());
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Konto väljavõtte laadimine ebaõnnestus");
        }
    }

    static String loadTransactions() {

        while (line != null) {
            try {
                FileReader fileReader = new FileReader("transactions.txt");
                BufferedReader bufferedReader = new BufferedReader(fileReader);
                transactions = bufferedReader.readLine();
                String line = bufferedReader.readLine();
                bufferedReader.close();
                fileReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        return transactions;

    }
    static void savePin(String pin) {
        // Toimub pin kirjutamine pin.txt faili
        try {
            FileWriter fileWriter = new FileWriter("pin.txt");
            fileWriter.write(pin + System.lineSeparator());
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Pin koodi salvestamine ebaõnnestus");
        }
    }

    static String loadPin() {
        // Toimub pin välja lugemine pin.txt failist
        // toimub Shadowing
        //Shadowing refers to the practice in Java programming
        // of using two variables with the same name within scopes that overlap.
        // When you do that, the variable with the higher-level scope is hidden
        // because the variable with lower-level scope overrides it.
        // The higher-level variable is then “shadowed.”
        String pin = null;
        try {
            FileReader fileReader = new FileReader("pin.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            pin = bufferedReader.readLine();
            bufferedReader.close();
            fileReader.close();
        } catch (IOException e) {
            System.out.println("Pin koodi laadimine ebaõnnestus");
        }
        return pin;
    }

    static void saveBalance(int balance) {
        // Toimub balance kirjutamine balance.txt faili
        try {
            FileWriter fileWriter = new FileWriter("balance.txt");
            fileWriter.write(balance + System.lineSeparator());
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Kontojäägi salvestamine ebaõnnestus");
        }

    }

    static int loadBalance() {
        // Toimub balance välja lugemine balance.txt failist
        int balance = 0;
        try {
            FileReader fileReader = new FileReader("balance.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            balance = Integer.parseInt(bufferedReader.readLine());
            bufferedReader.close();
            fileReader.close();
        } catch (IOException e) {
            System.out.println("Kontojäägi laadimine ebaõnnestus");
        }
        return balance;
    }

    static boolean validatePin() {
        Scanner scanner = new Scanner(System.in);
        pin = loadPin();

        for (int i = 0; i < 3; i++) {
            System.out.println("Palun sisesta PIN kood");

            String enteredPin = scanner.nextLine();

            if(enteredPin.equals(pin)) {
                System.out.println("Tore! Õige pin kood");
                return true;
            }
        }
        return false;

    }
}
