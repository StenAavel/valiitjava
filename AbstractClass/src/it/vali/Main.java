package it.vali;

public class Main {

    public static void main(String[] args) {
        // Abstraktne klass on hübriid liidesest ja klassist
        // Selles klassis saab defineerida nii meetodite struktuure (nagu liideses)
        // kui ka defineerida meetodi koos sisuga
        // Abstraktsest klassist ei saa otse objekti luua, saab ainult pärineda

        ApartmentKitchen kitchen = new ApartmentKitchen();
        kitchen.setHeight(100);
        kitchen.becomeDirty();
    }
}
