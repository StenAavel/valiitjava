package it.vali;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Farm implements LivingPlace {
    private List<FarmAnimal> animals = new ArrayList<FarmAnimal>();
    // Siin hoitakse infot, palju meil igat looma farmis on
    private Map<String, Integer> animalCounts = new HashMap<String, Integer>();

    // Siin hoitakse infot, palju meil igat looma farmis mahub
    private Map<String, Integer> maxAnimalCounts = new HashMap<String, Integer>();

    public Farm() {
        maxAnimalCounts.put("Pig", 5);
        maxAnimalCounts.put("Cow", 3);
        maxAnimalCounts.put("Sheep", 15);
    }
    @Override
    public void addAnimal(Animal animal) {
        // Kas animal on tüübist FarmAnimal või pärineb sellest tüübist
        if(!FarmAnimal.class.isInstance(animal)) {
            System.out.println("Farmis saavad elada ainult farmi loomad");
            return;
        }

        String animalType = animal.getClass().getSimpleName();


        if(!maxAnimalCounts.containsKey(animalType)) {
            System.out.println("Farmis selliseid loomi üldse ei ole");
            return;
        }

        if(animalCounts.containsKey(animalType)) {
            int animalCount = animalCounts.get(animalType);
            int maxAnimalCount = maxAnimalCounts.get(animalType);
            if(animalCount >= maxAnimalCount) {
                System.out.println("Farmis on sellele loomale kõik kohad juba täis");
                return;
            }
            animalCounts.put(animalType, animalCounts.get(animalType) + 1);
            // Sellist looma veel ei ole farmis
            // kindlasti sellele loomale kohta on
        } else {
            animalCounts.put(animalType, 1);
        }
        animals.add((FarmAnimal)animal);
        System.out.printf("Farmi lisati loom %s%n", animal);





//        System.out.println(animalType);


    }

    // Tee meetod mis prindib välja kõik farmis elavad loomad ja kui palju neid on
    // Pig 2
    // Cow 3 jne.
    @Override
    public void printAnimalCounts() {
        for (Map.Entry<String, Integer> entry : animalCounts.entrySet()    ) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }
    }
    // Tee meetod mis eemaldab farmist looma
    @Override
    public void removeAnimal(String animalType) {
        // Teen muutuja, mis listist ma tahan, et element oleks : list mida tahan läbi käia
        // FarmAnimal animal hakkab olema järjest esimene, teine jne. loom seni kuni loomi on
        // See kustutab hetkel ära kõik sead
        // Kui ei taha kõiki kustutada, tuleb lõppu lisada break;
        boolean animalFound = false;

        for (FarmAnimal animal : animals) {
            if(animal.getClass().getSimpleName().equals(animalType)) {
                animals.remove(animal);
                System.out.printf("Farmist eemaldati loom %s%n", animalType);
                // Kui see oli viimane loom, siis eemalda see rida animalCounts mapist
                // muul juhul vähenda animalCounts mapis seda kogust
                if(animalCounts.get(animalType)==1) {
                    animalCounts.remove(animalType);
                } else {
                    animalCounts.put(animalType, animalCounts.get(animalType) - 1);
                }
                animalFound = true;
                break;
            }

        }
        if(!animalFound) {
            System.out.println("Farmis antud loom puudub");
        }
        // Täienda meetodit nii, et kui ei leitud ühtegi sellist looma, prinditakse "Farmis selline loom puudub"
    }

}
