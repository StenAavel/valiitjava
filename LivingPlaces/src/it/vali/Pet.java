package it.vali;

public class Pet extends DomesticAnimal {
    private boolean sleepsIndoors = true;

    public void hasOwner() {
        System.out.println("Kuulun omanikule");
    }

    @Override
    public void printInfo() {
        super.printInfo();
        System.out.printf("Magab sees: %s%n", sleepsIndoors);
    }
}
