package it.vali;

public class Main {

    public static void main(String[] args) {
        LivingPlace livingPlace = new Zoo();

        Pig pig = new Pig();

        livingPlace.addAnimal(new Cat());
        livingPlace.addAnimal(new Horse());
        livingPlace.addAnimal(new Pig());
        livingPlace.addAnimal(pig);
        livingPlace.addAnimal(new Pig());
        livingPlace.addAnimal(new Pig());
        livingPlace.addAnimal(new Pig());
        livingPlace.addAnimal(new Pig());
        Cow cow = new Cow();
        livingPlace.addAnimal(cow);

        livingPlace.printAnimalCounts();

        livingPlace.removeAnimal("Cow");
        livingPlace.removeAnimal("Pig");
        livingPlace.printAnimalCounts();
        livingPlace.removeAnimal("Pig");
        livingPlace.removeAnimal("Pig");
        livingPlace.removeAnimal("Pig");
        livingPlace.removeAnimal("Pig");
        livingPlace.removeAnimal("Pig");
        livingPlace.printAnimalCounts();

        LivingPlace zoo = new Zoo();
        CarnivoreAnimal lion = new CarnivoreAnimal();
        HerbivoreAnimal gazelle = new HerbivoreAnimal();
        CarnivoreAnimal cheetah = new CarnivoreAnimal();
        zoo.addAnimal(lion);
        zoo.addAnimal(gazelle);
        zoo.addAnimal(cheetah);
        zoo.printAnimalCounts();

        Bear bear = new Bear();
        LivingPlace forest = new Forest();
        forest.addAnimal(bear);

    }
    // Mõelge ja täiendage Zoo ja Forest klasse nii, et neil oleks nende 3 meetodi sisud (kodutöö)

}