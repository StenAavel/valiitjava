package it.vali;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Zoo implements LivingPlace {
    private List<Animal> beasts = new ArrayList<Animal>();

    private Map<String, Integer> animalCounts = new HashMap<String, Integer>();

    private Map<String, Integer> maxAnimalCounts = new HashMap<String, Integer>();

    public Zoo() {
        maxAnimalCounts.put("Lion", 5);
        maxAnimalCounts.put("Gazelle", 15);
        maxAnimalCounts.put("Cheetah", 6);
    }

    @Override
    public void addAnimal(Animal animal) {
        if (Pet.class.isInstance(animal)) {
            System.out.println("Selles loomaaias lemmikloomi ei ole");
            return;

        }

        String animalType = animal.getClass().getSimpleName();


        if(!maxAnimalCounts.containsKey(animalType)) {
            System.out.println("Loomaaias selliseid loomi üldse ei ole");
            return;
        }

        if(animalCounts.containsKey(animalType)) {
            int animalCount = animalCounts.get(animalType);
            int maxAnimalCount = maxAnimalCounts.get(animalType);
            if(animalCount >= maxAnimalCount) {
                System.out.println("Loomaaias on sellele loomale kõik kohad juba täis");
                return;
            }
            animalCounts.put(animalType, animalCounts.get(animalType) + 1);
        } else {
            animalCounts.put(animalType, 1);
        }
        beasts.add((WildAnimal)animal);
        System.out.printf("Loomaaeda lisati loom %s%n", animal);
    }

    @Override
    public void printAnimalCounts() {
        for (Map.Entry<String, Integer> entry : animalCounts.entrySet()    ) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }

    }

    @Override
    public void removeAnimal(String animalType) {
        boolean animalFound = false;

        for (Animal animal : beasts) {
            if(animal.getClass().getSimpleName().equals(animalType)) {
                beasts.remove(animal);
                System.out.printf("Loomaaiast eemaldati loom %s%n", animalType);
                // Kui see oli viimane loom, siis eemalda see rida animalCounts mapist
                // muul juhul vähenda animalCounts mapis seda kogust
                if(animalCounts.get(animalType)==1) {
                    animalCounts.remove(animalType);
                } else {
                    animalCounts.put(animalType, animalCounts.get(animalType) - 1);
                }
                animalFound = true;
                break;
            }

        }
        if(!animalFound) {
            System.out.println("Loomaaias antud loom puudub");
        }

    }
}
