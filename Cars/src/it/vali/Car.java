package it.vali;

import java.util.ArrayList;
import java.util.List;

enum Fuel { GAS, PETROL, DIESEL, HYBRID, ELECTRIC }

public class Car {
    private String make;
    private String model;
    private int year;
    private Fuel fuel;
    private boolean isUsed;
    private boolean isEngineRunning;
    private int speed;
    private int maxSpeed;
    private Person driver;
    private Person owner;
    private Person passengerOne;
    private Person passengerTwo;
    private int maxPassengers;
    private Person person;

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public int getMaxPassengers() {
        return maxPassengers;
    }

    public void setMaxPassengers(int maxPassengers) {
        this.maxPassengers = maxPassengers;
    }

    private List<Person> passengers = new ArrayList<Person>();





    public Person getDriver() {
        return driver;
    }


    public void setDriver(Person driver) {
        this.driver = driver;
    }


    public Person getOwner() {
        return owner;
    }

    public void setOwner(Person owner) {
        this.owner = owner;
    }

    // Konstruktor/constructor
    // on eriline meetod, mis käivitatakse klassist objekti loomisel
    public Car() {
        System.out.println("Loodi auto objekt");
        maxSpeed = 200;
        isUsed = true;
        fuel = Fuel.PETROL;
    }

    // Constructor overloading
    public Car(String make, String model, int year, int maxSpeed) {
        this.make = make;
        this.model = model;
        this.year = year;
        this.maxSpeed = maxSpeed;
    }

    public Car(String make, String model, int year, int maxSpeed, Fuel fuel, boolean isUsed) {
        this.make = make;
        this.model = model;
        this.year = year;
        this.maxSpeed = maxSpeed;
        this.fuel = fuel;
        this.isUsed = isUsed;
    }

    public Car(String make, String model, int year, boolean isUsed, Person driver, Person owner, int maxPassengers) {
        this.make = make;
        this.model = model;
        this.year = year;
        this.isUsed = isUsed;
        this.driver = driver;
        this.owner = owner;
        this.passengers = passengers;
    }


    public void startEngine() {
        if(!isEngineRunning) {
            isEngineRunning = true;
            System.out.println("Mootor käivitus");
        } else {
            System.out.println("Mootor juba töötab");
        }
    }

    public void stopEngine() {
        if(isEngineRunning) {
            isEngineRunning = false;
            System.out.println("Mootor seiskus");
        } else {
            System.out.println("Mootor ei töötanudki");
        }
    }

    public void accelerate(int targetSpeed) {
        if(!isEngineRunning) {
            System.out.println("Auto mootor ei tööta, ei saa kiirendada!");
        } else if(targetSpeed>maxSpeed) {
            System.out.println("Auto nii kiiresti ei sõida");
            System.out.printf("Auto maksimus kiirus on %d%n", maxSpeed);
        } else if(targetSpeed<0) {
            System.out.println("Auto kiirus ei saa olla negatiivne");
        } else if(targetSpeed<speed) {
            System.out.println("Auto ei saa kiirendada madalamale kiirusele");
        } else {
            speed = targetSpeed;
            System.out.printf("Auto kiirendas kuni kiiruseni %d km/h%n", targetSpeed);
        }
    }
    // Lisada slowDown meetod (int targetSpeed) (kodutöö)
    public void slowDown(int targetSpeed) {
        if(!isEngineRunning) {
            System.out.println("Auto mootor ei tööta");
        } else if(targetSpeed==0) {
            System.out.println("Auto on peatunud");
        }  else if(targetSpeed>speed) {
            System.out.println("Ei saa aeglustada hetkekiirusest kõrgemale kiirusele");
        } else if(targetSpeed<0) {
            System.out.println("Auto kiirus ei saa olla negatiivne");
        } else {
            speed = targetSpeed;
            System.out.printf("Auto aeglustas kiiruseni %d km/h%n", targetSpeed);
        }

    }
    // Lisada park() ning mis tegevused oleks vaja selleks teha (kutsu välja juba olemasolevad meetodid)
    // aeglustamine, mootori välja lülitamine
    public void park() {
       slowDown(0);
       stopEngine();
       System.out.println("Auto on pargitud");
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }
// Lisa autole parameetrid driver ja owner (tüübist Person) ja nendele siis get ja set meetodid (kodutöö)

    // Loo mõni auto objekt, kellel on siis määratud kasutaja ja omanik (kodutöö)
    // Prindi välja auto omaniku vanus (läbi Auto) (kodutöö)

    // Lisa autole võimalus hoida reisijaid
    // Lisa meetodid reisijate lisamiseks ja eemaldamiseks autost
    // Kontrolli ka, et ei lisaks rohkem reisijaid, kui mahub
    // Lisa autole max reisijate arv



    public void addPassengers(Person passenger) {
        if(passengers.size()<maxPassengers) {
            if(passengers.contains(passenger)) {
                System.out.printf("Autos juba on reisija %s%n", passenger.getFirstName());
                return;
            }
            passengers.add(passenger);
            System.out.printf("Autosse lisati reisija %s%n", passenger.getFirstName());
        } else {
            System.out.println("Autosse ei mahu enam reisijaid");
        }

    }

    public void removePassengers(Person passenger) {
        if(passengers.indexOf(passenger) != -1) {
            passengers.remove(passenger);
            System.out.printf("Autost eemaldati reisija %s%n", passenger.getFirstName());
        } else {
            System.out.println("Autos sellist reisijat pole");
        }

    }

    public void showPassengers() {
        // Foreach loop
        // Iga elemendi kohta listis passengers tekita objekt passenger
        // 1. kordus Person passenger on esimene reisija
        // 2. kordus Person passenger on teine reisija jne.
        for(Person passenger : passengers) {
            System.out.println(passenger.getFirstName());

        }
    }

}
