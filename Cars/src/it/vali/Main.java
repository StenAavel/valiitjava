package it.vali;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
    // Car() ongi konstruktori välja kutsumine
	Car bmw = new Car();
	bmw.startEngine();
	bmw.startEngine();
	bmw.stopEngine();
	bmw.stopEngine();

	bmw.accelerate(100);
	bmw.startEngine();
	bmw.accelerate(100);

	Car fiat = new Car();
	Car mercedes = new Car();
	Car opel = new Car("Opel", "Vectra", 1999, 205);
	opel.startEngine();
	opel.accelerate(205);

	Car porsche = new Car("Porsche", "911", 1993, 250, Fuel.PETROL, false);

	Person driver = new Person("Sten", "Aavel", Gender.MALE, 30);
	Person owner = new Person("Gunnar", "Aavel", Gender.MALE, 80);
	Person passengerOne = new Person("Joonas", "Murand", Gender.MALE, 30);
	Person passengerTwo = new Person("Marit", "Ausmees", Gender.FEMALE, 26);
	Person person = new Person("Jaan", "Latikas", Gender.MALE, 31);

	opel.slowDown(204);
	opel.accelerate(205);
	opel.park();

	Car mazda = new Car("Mazda", "Xedos9", 1999, true, driver, owner, 4);



	System.out.println(owner.getAge());

	mazda.setOwner(owner);
	mazda.setDriver(driver);
	System.out.printf("Sõiduki %s omaniku vanus on %d%n", mazda.getMake(), mazda.getOwner().getAge());

	mazda.setMaxPassengers(4);

        mazda.addPassengers(person);
        mazda.addPassengers(driver);

        mazda.showPassengers();

        mazda.removePassengers(person);
        mazda.showPassengers();

        mazda.addPassengers(driver);

        Person juku = new Person("Juku", "Juurikas", Gender.MALE, 23);
        System.out.println(juku);

    }
}
