package it.vali;

import java.io.PrintStream;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // Kõigepealt küsitakse külastaja nime
        // kui nimi on listis, siis öeldakse kasutajale
        // "Tere tulemast, Sten"
        // ja küsitakse külastaja vanust
        // kui kasutaja on alaealine
        // teavitatakse teda, et ta sisse ei saa
        // muul juhul öeldakse tere tulemast klubisse

        // Kui kasutaja ei olnud listis,
        // küsitakse kasutajalt ka tema perekonnanime
        // kui perekonnanimi on listis siis öeldakse tere tulemast Sten Aavel
        // muul juhul öeldakse "ma ei tunne sind"
        String listFirstName = "Sten";
        String listLastName = "Aavel";

        Scanner scanner = new Scanner(System.in);

        System.out.println("Mis teie nimi on?");
        String firstName = scanner.nextLine();
        if (listFirstName.equals(firstName)) {
            System.out.printf("Tere tulemast %s%n", listFirstName);
            System.out.println("Kui vana te olete?");
            int a = Integer.parseInt(scanner.nextLine());
            if (a >= 18) {
                System.out.println("Tere tulemast klubisse");
            } else {
                System.out.println("Teie sisse ei saa");
            }
        } else {
            System.out.println("Mis on sinu perekonnanimi?");
            String lastName = scanner.nextLine();
            if (listLastName.equals(lastName)) {
            System.out.printf("Tere %s sugulane%n", listFirstName);
        }
            else {
            System.out.println("Sorry, ma ei tunne sind");
        }
        }
        }
}