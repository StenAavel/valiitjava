package it.vali;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static java.util.Calendar.MONTH;

public class Main {

    public static void main(String[] args) {
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss EEEEE");
        Calendar calendar = Calendar.getInstance();

        // Move calednar to yesterday
        calendar.add(Calendar.DATE, -1);

        Date date = calendar.getTime();

        System.out.println(dateFormat.format(date));

        // Get current date of calendar which point to the yesterday now
        calendar.add(Calendar.YEAR, 1);
        date = calendar.getTime();
        System.out.println(dateFormat.format(date));

        // Prindi ekraanile selle aasta järele jäänud kuude esimese kuupäeva nädalapäevad
        System.out.println(calendar.get(Calendar.MONTH));

        calendar.set(Calendar.YEAR, calendar.get(Calendar.MONTH), 1);
        date = calendar.getTime();
        int monthsLeftThisYear = 11 - calendar.get(Calendar.MONTH);
        DateFormat weekdayFormat = new SimpleDateFormat("EEEE");
//        for (int i = 0; i < monthsLeftThisYear; i++) {
//            calendar.add(Calendar.MONTH, 1);
//            date = calendar.getTime();
//            System.out.println(weekdayFormat.format(date));
//
//        }

        int currentYear = calendar.get(Calendar.YEAR);

        calendar.add(Calendar.MONTH, 1);

        while(calendar.get(Calendar.YEAR) == currentYear) {

            date = calendar.getTime();
            System.out.println(weekdayFormat.format(date));
            calendar.add(Calendar.MONTH, 1);
        }

    }
}
