package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        // Küsime kasutajalt PIN-koodi
        // kui see on õige, siis ütleme "ok"
        // muul juhul küsime uuesti
        // aga kokku 3 korda
        for (int i = 0; i < 3 ; i++) {
        System.out.println("Sisestage oma PIN-kood");
        Scanner scanner = new Scanner(System.in);
        String realPin = "6767";
        String enterPin = scanner.nextLine();

        if(enterPin.equals(realPin)) {
            System.out.println("Kood on õige");
            break;
            // break hüppab tsüklist välja
        }
    }

        Scanner scanner = new Scanner(System.in);
        String realPin = "6767";
        String enterPin = scanner.nextLine();

        int retriesLeft = 3;
// incomplete
        do {
            System.out.println("Sisestage oma PIN-kood");
            retriesLeft--;

        } while (!scanner.nextLine().equals(realPin) && retriesLeft > 0);
        System.out.println("Kood on õige");
        // inco
        if(realPin.equals(enterPin)) {
            System.out.println("Panid 3 korda valesti");
        }
        else {
            System.out.println("Tore! Õige PIN-kood");
        }

        // prindi välja 10 kuni 20 ja 40 kuni 60

        // continue jätab selle tsüklikorduse katki ja läheb järgmise korduse juurde
        for (int i = 10; i <= 60 ; i++) {
            if(i>20 && i<40) {
                continue;
            }
            System.out.println(i);
        }

    }
}
