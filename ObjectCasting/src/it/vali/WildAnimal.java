package it.vali;

public class WildAnimal extends Animal {

    private boolean isDeadly = false;

    public boolean isDeadly() {
        return isDeadly;
    }

    public void setDeadly(boolean deadly) {
        isDeadly = deadly;
    }
    public void dangerous() {
        System.out.println("Võin olla ohtlik");
    }

    @Override
    public String getBreed() {
        return "Metsloomal pole tõugu";
    }
}
