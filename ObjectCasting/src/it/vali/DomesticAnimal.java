package it.vali;

public class DomesticAnimal extends Animal {
    private String name;
    private String age;
    private double weight;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void friendly() {
        System.out.println("Olen inimsõbralik");
    }

    public void printInfo() {
        System.out.printf("Tõug %s%n", getBreed());
        System.out.printf("Vanus %d%n", age);
        System.out.printf("Kaal %.2f%n", getWeight());
        System.out.printf("Looma nimi on %s%n", getName());
    }
}
