package it.vali;

public class HerbivoreAnimal extends WildAnimal {


    private boolean isEndangered = false;

    public boolean isEndangered() {
        return isEndangered;
    }

    public void setEndangered(boolean endangered) {
        isEndangered = endangered;
    }
    public void plants() {
        System.out.println("Söön taimi");
    }
}
