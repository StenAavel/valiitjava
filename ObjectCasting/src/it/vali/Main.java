package it.vali;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        // Numbreid peab castima teiseks, kui tahad suuremat väiksema sisse panna
        int a = 100;
        short b = (short)a;

        a = b;

        double c = a;
        a = (int)c;

        // Samamoodi on klassidega
        // Iga kassi võib võtta kui looma
        // Implicit casting:
        Animal animal = new Cat();

        // Explicit casting:
        // Iga loom ei ole kass
        Cat cat = (Cat) animal;

        List<Animal> animals = new ArrayList<Animal>();

        Dog dog = new Dog();
        dog.setName("Muki");
        Pig pig = new Pig();
        Cow cow = new Cow();

        animals.add(dog);
        animals.add(pig);
        animals.add(new Pet());
        animals.add(new Cat());
        ((Cat)animals.get(animals.size() - 1)).setWeight(20);
        animals.add(new WildAnimal());

        animals.get(animals.indexOf(dog)).setBreed("shiba");

        // Kui indeksit teada, saab parameetreid panna nii
        animals.get(3).setBreed("burmese");

        // Kutsu kõikide listis olevate loomade printInfo välja

        for (Animal animalInList: animals) {
            animalInList.printInfo();
            System.out.println();
        }

        for (Animal animalsInList: animals) {
            animalsInList.eat();
            System.out.println();
        }

        Animal secondAnimal = new Dog();

        ((Dog)secondAnimal).setHasTail(true);

        secondAnimal.printInfo();


    }
}
