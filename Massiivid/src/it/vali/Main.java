package it.vali;

public class Main {

    public static void main(String[] args) {
        // Järjend, massiiv, nimekiri
        // Ühes muutujas saab hoida sama tüüpi elemente

        // Luuakse täisarvude massiiv, millesse mahub viis elementi
        // Loomise hetkel määratud elementide arvu hiljem muuta ei saa
        int[] numbers = new int[5];

        // Massiivi indeksid algavad nullist, mitte ühest
        // viimane indeks on alati ühe võrra väiksem, kui massiivi pikkus
        numbers[0] = 2;
        numbers[1] = 7;
        numbers[2] = -2;
        numbers[3] = 11;
        numbers[4] = 1;

        System.out.println(numbers[0]);
        System.out.println(numbers[1]);
        System.out.println(numbers[2]);
        System.out.println(numbers[3]);
        System.out.println(numbers[4]);

        System.out.println();

        for (int i = 0; i < 5; i++) {
            System.out.println(numbers[i]);
        }
        System.out.println();

        for (int i = 4; i >= 0; i--) {
            System.out.println(numbers[i]);
        }
        System.out.println();

        // Prindi arvud mis on suuremad kui 2, prindi kõik paarisarvud, prindi tagant poolt esimesed kaks paaritut arvu
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] > 2) {
                System.out.println(numbers[i]);
            }
        }
        System.out.println();

        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] % 2 == 0) {
                System.out.println(numbers[i]);
            }
        }
        System.out.println();

        int counter = 0;

        for (int i = numbers.length - 1; i >= 0; i--) {
            if (numbers[i] % 2 != 0) {
                System.out.println(numbers[i]);
                counter++;
                if (counter == 2) {
                    break;
                }
            }
        }


        System.out.println();

        // Loo teine massiiv kolmele numbrile ja pane sinna esimesest
        // massiivist 3 esimest numbrit
        // Prindi teise massiivi elemendid ekraanile

        // Loo kolmas massiiv kolmele numbrile ja pane sinna esimesest
        // massiivist 3 viimast numbrit

        int[] secondNumbers = new int[3];

        secondNumbers[0] = numbers[0];
        secondNumbers[1] = numbers[1];
        secondNumbers[2] = numbers[2];

        for (int i = 0; i < secondNumbers.length; i++) {
            secondNumbers[i] = numbers[numbers.length - i - 1];
        }
//        // incomplete
//        for (int i = 0, i<secondNumbers.length)
//
//        secondNumbers[0] = numbers[4];
//        secondNumbers[1] = numbers[3];
//        secondNumbers[2] = numbers[2];

//        for (int i = 0; i < secondNumbers.length; i++) {
//            secondNumbers[i] = numbers[numbers.length - i - 1];
//        }
//
//        for (int i = 0; i < secondNumbers.length; i++) {
//            System.out.println(secondNumbers[i]);
//        }
//
//        int[] thirdNumbers = new int[3];
//
//        for (int i = 0, j = numbers.length -1; i < thirdNumbers.length; i++, j--) {
//            thirdNumbers[i] = numbers[j];
//        }
//        for (int i = 0; i < thirdNumbers.length; i++) {
//            System.out.println(thirdNumbers[i]);
        }

    }

