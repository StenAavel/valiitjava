package it.vali;

import java.util.List;

public class Language {
    private String languageName;
    private List<String> countrynames;

    public List<String> getCountrynames() {
        return countrynames;
    }

    public void setCountrynames(List<String> countrynames) {
        this.countrynames = countrynames;
    }

    @Override
    public String toString() {
        return String.join(", ", countrynames);
    }

    public String getLanguageName() {
        return languageName;
    }

    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }



}
