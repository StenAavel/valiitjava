package it.vali;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        // 1. Arvuta ringi pindala, kui teada on raadius. Prindi pindala ekraanile.
        double radius = 3.33;
        double c = Math.PI*radius*radius;
        System.out.printf("Ringi pindala on %.2f%n", c);

        // 2. Kirjuta meetod, mis tagastab boolean-tüüpi väärtuse ja mille sisendparameetriteks on kaks stringi.
        // Meetod tagastab kas tõene või vale selle kohta, kas stringid on võrdsed.

        // 3. Kirjuta meetod, mille sisendparameetriks on täisarvude massiiv ja mis tagastab stringide massiivi.
        // Iga sisend-massiivi elemendi kohta olgu tagastatavas massiivis sama palju a-tähti.
        // 3=aaa, 6=aaaaaa, 2=aa jne.

        // 4. Kirjuta meetod, mis võtab sisendparameetrina aastaarvu ja tagastab kõik sellel sajandil esinenud liigaastad.
        // Sisestada peab saama ainult aastaid vahemikus 500-2019.

        // 5. Defineeri klass Language, sellel klassil getLanguageName, setLanguageName ning  list riikide nimedega,
        // kus seda keelt räägitakse. Kirjuta üle selle klassi meetod toString() nii, et see tagastab riikide nimekirja
        // eraldades komaga. Tekita antud klassist 1 objekt ühe vabalt valitud keele andmetega ning prindi välja selle
        // objekti toString() meetodi sisu.

        Language language = new Language();
        language.setLanguageName("English");
        List<String> countryNames = new ArrayList<String>();
        countryNames.add("USA");
        countryNames.add("UK");
        countryNames.add("Austraalia");
        countryNames.add("Kanada");
        language.setCountrynames(countryNames);
        System.out.println(language.toString());


    }

    // 2. Kirjuta meetod, mis tagastab boolean-tüüpi väärtuse ja mille sisendparameetriteks on kaks stringi.
    // Meetod tagastab kas tõene või vale selle kohta, kas stringid on võrdsed.
    static boolean strings(String stringOne, String stringTwo) {
        if(stringOne.equals(stringTwo)) {
            return true;
        } return false;

    }

    // 3. Kirjuta meetod, mille sisendparameetriks on täisarvude massiiv ja mis tagastab stringide massiivi.
    // Iga sisend-massiivi elemendi kohta olgu tagastatavas massiivis sama palju a-tähti.
    // 3=aaa, 6=aaaaaa, 2=aa jne.
    public String[] fullNumbers (int[] numbers) {
        String[] words = new String[numbers.length];
        for (int i = 0; i < words.length; i++) {
            words[i] = generateAString(numbers[i]);
        } return words;
    }

    static String generateAString(int count) {
        String word = "";
        for (int i = 0; i < count; i++) {
            word += "a";
        }
        return word;
    }


    // 4. Kirjuta meetod, mis võtab sisendparameetrina aastaarvu ja tagastab kõik sellel sajandil esinenud liigaastad.
    // Sisestada peab saama ainult aastaid vahemikus 500-2019.

    static List<Integer> years (int year) {
        if(year<500 || year>2019) {
            System.out.println("Aasta peab olema 500 ja 2019 vahel");
            return null;
        }
        int leapYear = 2020;
        int centuryStart = year/100*100;
        int centuryEnd = centuryStart + 99;

        List<Integer> yearss = new ArrayList<Integer>();

        for (int i = 2020; i >= centuryStart ; i-=4) {
            if(i<= centuryEnd && i%4==0) {
                yearss.add(i);
            }
        } return yearss;




    }



}
