package it.vali;

public class Main {

    public static void main(String[] args) {
        int[] numbers = new int[]{-2, 4, -12, 161, 8, 12};

        int max = numbers[0];
        for (int i = 1; i < numbers.length; i++) {
            if (numbers[i] > max) {
                max = numbers[i];
            }
        }
        System.out.println(max);

        int min = numbers[0];
        for (int i = 1; i < numbers.length; i++) {
            if (numbers[i] < min) {
                min = numbers[i];
            }
        }
        System.out.println(min);

        // Leia suurim paaritu arv
        // mitmes number see on?
        max = Integer.MIN_VALUE;
        int position = 1;
        boolean oddNumbersFound = false;

        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] % 2 != 0 && numbers[i] > max) {
                max = numbers[i];
                position = i + 1;
                oddNumbersFound = true;
            }

        }
        if (oddNumbersFound) {
            System.out.printf("Suurim paaritu number on %d ja ta järjekorranumber on %d%n", max, position);
            // Kui paarituid arve üldse ei ole, prindi, et "paaritud arvud puuduvad"

        } else {
            System.out.println("Paaritud arvud puuduvad");
        }

        // Teine variant sama asja jaoks

        max = Integer.MIN_VALUE;
        position = -1;

        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] % 2 != 0 && numbers[i] > max) {
                max = numbers[i];
                position = i + 1;

            }

        }
        if (position != -1) {
            System.out.printf("Suurim paaritu number on %d ja ta järjekorranumber on %d%n", max, position);
            // Kui paarituid arve üldse ei ole, prindi, et "paaritud arvud puuduvad"

        } else {
            System.out.println("Paaritud arvud puuduvad");
        }
    }
}