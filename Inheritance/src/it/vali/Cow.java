package it.vali;

public class Cow extends FarmAnimal {
    private double averageMilkAmount;

    public double getAverageMilkAmount() {
        return averageMilkAmount;
    }

    public void setAverageMilkAmount(double averageMilkAmount) {
        this.averageMilkAmount = averageMilkAmount;
    }

    public void milk() {
        System.out.println("Annan piima");
    }
}
