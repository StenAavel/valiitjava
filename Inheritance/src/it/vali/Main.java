package it.vali;

public class Main {

    public static void main(String[] args) {
        Cat angora = new Cat();
        angora.setName("Miisu");
        angora.setAge(10);
        angora.setBreed("Angora");
        angora.setWeight(2.34);

        angora.printInfo();
        angora.eat();
        System.out.println();


        Cat persian = new Cat();
        persian.setName("Liisu");
        persian.setAge(1);
        persian.setBreed("Persian");
        persian.setWeight(3.11);

        persian.printInfo();
        persian.eat();
        System.out.println();

        Dog tax = new Dog();
        tax.setName("Muki");
        tax.setAge(3);
        tax.setBreed("Tax");
        tax.setWeight(3.22);

        CarnivoreAnimal wolf = new CarnivoreAnimal();
        wolf.setAge(5);
        wolf.setBreed("Grey Wolf");
        wolf.setWeight(30.2);
        wolf.hunt();

        wolf.printInfo();

        System.out.println();

        FarmAnimal sheep = new FarmAnimal();
        sheep.setAge(6);
        sheep.setBreed("Regular Sheep");
        sheep.setWeight(45.3);
        sheep.home();

        sheep.printInfo();

        System.out.println();

        DomesticAnimal horse = new DomesticAnimal();
        horse.setAge(8);
        horse.setBreed("Stallion");
        horse.setWeight(84);
        horse.setName("Suksu");

        horse.printInfo();


        System.out.println();

        tax.eat();

        persian.catchMouse();
        tax.playWithCat(persian);

        // Lisa pärinevusahelast puuduvad klassid
        // Igale klassile lisa 1 muutuja ja 1 meetod, mis on ainult sellele klassile omane (kodus lõpetada)
    }

}
