package it.vali;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Main {

    static String pin;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        do {
            pin = loadPin();

            // Hoiame pin koodi failis pin.txt
            // ja kontojääki balance.txt

            if (validatePin() == true) {
                System.out.println("Millist tehingut soovite?");
                System.out.println("a Raha välja võtmine -20EUR");
                System.out.println("b Raha välja võtmine -100EUR");
                System.out.println("c Raha sissemakse +20EUR");
                System.out.println("d Raha sissemakse +100EUR");
                System.out.println("e PIN-koodi muutmine");
                System.out.println("f Kontojäägi vaatamine");
                System.out.println("g Katkesta");
                String newCommand = scanner.nextLine();
                if (newCommand.equals("a")) {
                    int balance = loadBalance();
                    loadBalance();
                    int newBalance = balance - 20;
                    saveBalance(newBalance);
                    System.out.printf("Raha väljastatud. Uus kontojääk on %d%n", newBalance);
                } else if (newCommand.equals("b")) {
                    int balance = loadBalance();
                    int newBalance = balance - 100;
                    saveBalance(newBalance);
                    System.out.printf("Raha väljastatud. Uus kontojääk on %d%n", newBalance);
                } else if (newCommand.equals("c")) {
                    int balance = loadBalance();
                    int newBalance = balance + 20;
                    saveBalance(newBalance);
                    System.out.printf("Raha sisestatud. Uus kontojääk on %d%n", newBalance);
                } else if (newCommand.equals("d")) {
                    int balance = loadBalance();
                    int newBalance = balance + 100;
                    saveBalance(newBalance);
                    System.out.printf("Raha sisestatud. Uus kontojääk on %d%n", newBalance);
                } else if (newCommand.equals("e")) {
                    Scanner newpinscanner = new Scanner(System.in);
                    System.out.println("Uus pin: ");
                    String newpin = scanner.nextLine();
                    savePin(newpin);
                    System.out.println("Pin-kood muudetud.");
                } else if (newCommand.equals("f")) {
                    int balance = loadBalance();
                    System.out.printf("Teie kontojääk on %d%n", balance);
                } else if (newCommand.equals("g")){
                    System.out.println("Tänan kasutamast meie teenust. Head päeva.");
                }
                else {
                    System.out.println("Esines viga. Palun alustage uuesti.");
                }


                int balance = loadBalance();


                System.out.println("Kas soovid uut tehingut? j/e");
                // Lisa konto väljavõtte funktsionaalsus
//        System.out.println(currentDateTimeToString() + " Sularaha väljavõtt -100€");
            }

        } while (scanner.nextLine().equals("j"));
    }
    static String currentDateTimeToString() {
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss EEEEE");

        Date date = new Date();
        return dateFormat.format(date);
    }

    static void savePin(String pin) {
        // Toimub pin kirjutamine pin.txt faili
        try {
            FileWriter fileWriter = new FileWriter("pin.txt");
            fileWriter.write(pin + System.lineSeparator());
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Pin koodi salvestamine ebaõnnestus");
        }
    }

    static String loadPin() {
        // Toimub pin välja lugemine pin.txt failist
        // toimub Shadowing
        //Shadowing refers to the practice in Java programming
        // of using two variables with the same name within scopes that overlap.
        // When you do that, the variable with the higher-level scope is hidden
        // because the variable with lower-level scope overrides it.
        // The higher-level variable is then “shadowed.”
        String pin = null;
        try {
            FileReader fileReader = new FileReader("pin.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            pin = bufferedReader.readLine();
            bufferedReader.close();
            fileReader.close();
        } catch (IOException e) {
            System.out.println("Pin koodi laadimine ebaõnnestus");
        }
        return pin;
    }

    static void saveBalance(int balance) {
        // Toimub balance kirjutamine balance.txt faili
        try {
            FileWriter fileWriter = new FileWriter("balance.txt");
            fileWriter.write(balance + System.lineSeparator());
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Kontojäägi salvestamine ebaõnnestus");
        }

    }

    static int loadBalance() {
        // Toimub balance välja lugemine balance.txt failist
        int balance = 0;
        try {
            FileReader fileReader = new FileReader("balance.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            balance = Integer.parseInt(bufferedReader.readLine());
            bufferedReader.close();
            fileReader.close();
        } catch (IOException e) {
            System.out.println("Kontojäägi laadimine ebaõnnestus");
        }
        return balance;
    }

    static boolean validatePin() {
            Scanner scanner = new Scanner(System.in);

            for (int i = 0; i < 3; i++) {
                System.out.println("Palun sisesta PIN kood");

                String enteredPin = scanner.nextLine();

                if(enteredPin.equals(pin)) {
                    System.out.println("Tore! Õige pin kood");
                    return true;
                }
            }
            return false;
    }

}

