﻿-- See on lihtne Hello World tekstipäring, mis tagastab ühe rea ja ühe veeru (veerul puudub pealkiri)
-- Selles veerus ja reas saab olema tekst Hello World
SELECT 'Hello World';

-- Täisarvu saab küsida ilma '' märkideta
SELECT 3;

-- ei tööta PostgreSQL-is. Nt. MSSql-is töötab.
SELECT 'raud' + 'tee';

-- Standard CONCAT töötab kõigis erinevates SQL serverites
SELECT concat('all','maa','raud','tee','jaam','.',2,0,0,4);

-- Kui tahan erinevaid veerge, siis panen väärtustele koma vahele
SELECT 'Peeter', 'Paat', 23, 75.45, 'blond'

-- AS märksõnaga saab anda antud veerule nime
SELECT 
'Peeter' AS eesnimi, 
'Paat' AS perekonnanimi, 
23 AS vanus, 
75.45 AS kaal, 
'blond' AS juuksevärv

-- Tagastab praeguse kellaaja ja kuupäeva mingis vaikimisi formaadis
SELECT NOW() AS kuupäev;
-- Kui tahan konkreetset osa sellest, näiteks aastad või kuud
SELECT date_part('year', NOW());

-- Kuupäeva formaatimine Eesti süsteemile
SELECT to_CHAR(NOW(), 'HH24:mi:ss DD.MM.YYYY')

-- Interval laseb lisada või eemaldada mingit ajaühikut
SELECT now() + interval '1 day ago';
SELECT now() - interval '1 day';
SELECT now() + interval '2 centuries 3 years 2 months 1 weeks 3 days 4 seconds';

-- Tabeli loomine
CREATE TABLE student (
   id serial PRIMARY KEY, 
	-- serial tähendab, et tüübiks on int, mis hakkab ühe võrra suurenema
	-- PRIMARY KEY (primaarvõti) tähendab, et see on unikaalne väli tabelis
	first_name varchar(64) NOT NULL, -- Ei tohi tühi olla
	last_name varchar(64) NOT NULL,
	age int  NULL, -- Tohib tühi olla
	height int NULL, 
	weight numeric(5, 2) NULL,
	birthday date NULL
);

-- Tabelist kõikide ridade ja veergude küsimine
-- * tähendab, et anna kõik veerud
SELECT * FROM student;

SELECT first_name FROM student;

SELECT first_name, age FROM student;

SELECT first_name AS eesnimi, 
age AS vanus 
FROM 
student;

-- Kui tahan mingit osa ise ette antud kuupäevast
SELECT date_part('month', TIMESTAMP '2019-01-01');

-- Kui tahan saada mingit osa ise antud kellaajast
SELECT date_part('minutes', TIME '10:10');

-- Sümbolid lähevad märgitud numbrite vahele
SELECT to_char(NOW(), 'HH24:mi:ss DD,,,MM...YYYY');

SELECT TIMESTAMP '2000-01-01' + interval '1 days';

SELECT CONCAT(first_name, ' ', lsdt_name, ' ', date_part('year', birthday)) AS täisnimi FROM student;

-- Kui tahan filtreerida/otsida mingi tingimuse järgi
SELECT 
* 
FROM 
student
WHERE
height = 180;

SELECT 
* 
FROM 
student
WHERE
first_name = 'Peeter';

SELECT 
* 
FROM 
student
WHERE
first_name = 'Peeter'
AND
lsdt_name = 'Puujalg';

SELECT 
* 
FROM 
student
WHERE
first_name = 'Peeter'
OR
lsdt_name = 'Maasikas';

-- Kui tahad kahte inimest nii ees- kui perekonnanime järgi
SELECT 
* 
FROM 
student
WHERE
(first_name = 'Peeter'
AND lsdt_name = 'Puujalg')
OR
(first_name = 'Mari' AND lsdt_name = 'Maasikas');

-- Anna mulle õpilased, kelle pikkus jääb 170 ja 180 cm vahele
SELECT 
* 
FROM 
student
WHERE
height >= 170
AND
height <= 180;

-- Anna mulle õpilased, kes on pikemad kui 170 või lühemad kui 150
SELECT 
* 
FROM 
student
WHERE
height > 170
OR
height < 150;

-- Anna mulle õpilased, kellel on sünnipäev mais
SELECT 
* 
FROM 
student
WHERE
date_part('month', birthday) = 5;

-- Anna mulle õpilaste eesnimed ja pikkused, kellel on sünnipäev mais
SELECT 
first_name, height
FROM 
student
WHERE
date_part('month', birthday) = 5;

-- Anna mulle õpilased, kelle middle_name on/ei ole null (määramata)
SELECT
*
FROM
student
WHERE
middle_name IS NULL;

SELECT
*
FROM
student
WHERE
middle_name IS NOT NULL;

-- Anna õpilased, kelle pikkus ei ole 180 cm
SELECT
*
FROM
student
WHERE
height != 180;

-- Anna õpilased, kelle pikkus on 160, 170 või 180
SELECT
*
FROM
student
WHERE
-- height = 160 OR height = 170 OR height = 180;
height IN (160, 170, 180);

-- Anna mulle õpilased, kelle eesnimi on Peeter, Mari või Kalle
SELECT
*
FROM
student
WHERE
first_name IN ('Peeter', 'Mari', 'Kert');

-- Anna mulle õpilased, kelle eesnimi ei ole Peeter, Mari või Kalle
SELECT
*
FROM
student
WHERE
first_name NOT IN ('Peeter', 'Mari', 'Kert');

-- Anna mulle õpilased, kelle sünnikuupäev on kuu 1., 4. või 7. päeval
SELECT
*
FROM
student
WHERE
date_part('day', birthday) IN (1, 4, 7);

-- Kõik WHERE võrdlused jätavad välja NULL väärtusega read
SELECT
*
FROM
student
WHERE
age > 0 OR age <= 0 OR age IS NULL;

-- Anna mulle õpilased pikkuse järjekorras lühemast pikemaks
SELECT
*
FROM
student
ORDER BY
height;

-- Anna mulle õpilased pikkuse järjekorras lühemast pikemaks
-- Kui pikkused on võrdsed, järjesta kaalu järgi
SELECT
*
FROM
student
ORDER BY
height, weight;

-- Kui tahan tagurpidises järjekorras, siis lisandub sõna DESC
-- On olemas ka ASC
SELECT
*
FROM
student
ORDER BY
first_name DESC, lsdt_name DESC;

-- Anna mulle vanuse järjekorras vanemast nooremaks õpilaste pikkused, mis jäävad
-- 160 ja 170 vahele
SELECT
height
FROM
student
WHERE
height >= 160 AND height <= 170
ORDER BY
age DESC

-- Kuidas leida konkreetset tähte sõnas:
-- s% - s sõna esimese tähena
-- %s - s sõna viimase tähena
-- %s% - s sõna sees
-- Suur ja väike täht loevad!
SELECT
*
FROM
student
WHERE
first_name LIKE '%s';

-- Tähekombinatsioonide otsimine
SELECT
*
FROM
student
WHERE
first_name LIKE 'Jo%'
OR first_name LIKE '%nn%'
OR first_name LIKE '%es';

-- Tabelisse uute kirjete lisamine
INSERT INTO student
(first_name, lsdt_name, height, weight, birthday, middle_name)
VALUES
('Voldemar', 'Kuslap', 90, 40, '1937-12-02', NULL),
('Gunnar', 'Nurme', 179, 80, '1956-02-13', 'Hendrik'),
('Silvi', 'Sarapuu', 191, 68, '1987-11-29', NULL)

-- Tabelis kirje muutmine
-- UPDATE lausega peab olema ettevaatlik
-- Alati peab kasutama WHERE-i lause lõpus
UPDATE
student
SET
lsdt_name = 'Täismees'
WHERE
first_name = 'Johannes'

UPDATE
student
SET
height = 199,
weight = 198
WHERE
first_name = 'Johannes';

-- Muuda kõigi õpilaste pikkust ühe võrra suuremaks
UPDATE
student
SET
height = height + 1;

-- Suurenda hiljem kui 1999 sündinud õpilaste sünnipäeva ühe päeva võrra
UPDATE
student
SET
birthday = birthday + interval '1 day'
WHERE
date_part('year', birthday) > 1999;

-- Kustutamisel olla ettevaatlik. ALATI KASUTA WHERE-i.
DELETE FROM
student
WHERE
id = 11;

-- Andmete CRUD operations
-- Create (Insert), Read (Select), Update, Delete
-- Kõik eelpool mainitud operatsioonid

-- Loo uus tabel loan, millel on väljad: amount (reaalarv), start_date, due_date, 
-- student_id (hakkab vastama konkreetse studenti id-ga student tabelist).
CREATE TABLE loan (
   id serial PRIMARY KEY, 
	amount numeric (11, 2) NOT NULL,
	start_date date NOT NULL,
	due_date date NOT NULL,
	student_id int NOT NULL
);

-- Lisa kolmele õpilasele laenud. Kahele neist lisa veel üks laen.
INSERT INTO loan
(amount, start_date, due_date, student_id)
VALUES
(5000, '2019-05-14', '2029-05-14', 1),
(15000, '2019-05-14', '2034-05-14', 2),
(1000, '2019-05-14', '2029-07-14', 3),
(2000, '2019-07-14', '2020-07-14', 1),
(4500, '2019-05-30', '2023-06-01', 3);

-- Anna mulle kõik õpilased koos oma laenudega
SELECT
loan.*, student.* -- Kui panna lihtsalt tärn annab mõlemad tabelid koos, saab ka eraldi
FROM
student
JOIN
loan
ON student.id = loan.student_id;

-- Anna mulle kõik õpilased koos oma laenudega
SELECT
student.first_name, -- Nii saab valida ainult konkreetsed andmed mõlemast tabelist
student.lsdt_name,
loan.amount
FROM
student
JOIN
loan
ON student.id = loan.student_id;

-- Anna mulle kõik õpilased koos oma laenudega
-- aga ainult sellised laenud, mis on suuremad kui 500
-- järjesta laenu suuruse järgi
-- INNER JOIN on selline tabelite liitmine, kus liidetakse ainult need read,
-- kus on võrdsed student.id = loan.student_id
-- ehk need read, kus tabelite vahel on seos.
-- Ülejäänud ridu ignoreeritakse.
SELECT
student.first_name,
student.lsdt_name,
loan.amount
FROM
student
INNER JOIN -- INNER JOIN on vaikimisi JOIN, INNER sõna võib ära jätta
loan
ON student.id = loan.student_id
WHERE
loan.amount > 500
ORDER BY 
loan.amount DESC;

-- Loo uus tabel loan_type, milles on väljad name ja description
CREATE TABLE loan_type (
id serial PRIMARY KEY,
	name varchar(30) NOT NULL,
	description varchar(200) NULL
);

-- Uue välja lisamine olemasolevale tabelile
ALTER TABLE loan
ADD COLUMN loan_type_id int

-- Tabeli kustutamine
DROP TABLE student;

-- Kolme tabeli Inner Join
SELECT 
s.first_name, s.lsdt_name, l.amount, lt.name
FROM
student AS s
JOIN
loan AS l
ON s.id = l.student_id
JOIN
loan_type AS lt
ON lt.id = l.loan_type_id;

-- Joinimiste järjekord pole oluline
SELECT 
s.first_name, s.lsdt_name, l.amount, lt.name
FROM
loan_type AS lt
JOIN 
loan AS l
ON
l.loan_type_id = lt.id
JOIN
student AS s
ON
s.id = l.student_id;

-- Kui tahad tabelis kuvada ka neid, kes ei võtnud laenu
-- LEFT JOIN puhul võetakse joini esimesest (vasakust, student) tabelist kõik read
-- ning teises tabelis (paremas, loan) näidatakse puuduvatel kohtadel NULL
SELECT 
s.first_name, s.lsdt_name, l.amount
FROM
student AS s
LEFT JOIN
loan AS l
ON s.id = l.student_id;

-- LEFT JOIN puhul on järjekord väga oluline
SELECT 
s.first_name, s.lsdt_name, l.amount, lt.name
FROM
student AS s
LEFT JOIN
loan AS l
ON s.id = l.student_id
LEFT JOIN
loan_type AS lt
ON lt.id = l.loan_type_id;

-- Annab kõik kombinatsioonid kahe tabeli vahel
SELECT 
s.first_name, st.first_name
FROM
student AS s
CROSS JOIN
student AS st
WHERE
s.first_name != st.first_name


-- FULL OUTER JOIN on sama mis LEFT JOIN + RIGHT JOIN
SELECT 
s.first_name, s.lsdt_name, l.amount
FROM
student AS s
FULL OUTER JOIN
loan AS l
ON s.id = l.student_id;

-- Anna mulle kõigi kasutajate perekonnanimed, kes võtsid SMS-laenu
-- ja on võlgu üle 100 euro. Tulemused järjesta laenuvõtja vanuse järgi
-- väiksemast suuremaks.
SELECT
s.lsdt_name
FROM
student AS s
JOIN
loan AS l
ON
s.id = l.student_id
JOIN
loan_type AS lt
ON
lt.id = l.loan_type_id
WHERE
lt.name = 'SMS laen'
AND l.amount > 100
ORDER BY
s.birthday

-- Agregaatfunktsioonid
-- Agregaatfunktsiooni selectis välja kutsudes kaob võimalus samas select lauses
-- küsida mingit muud väärtust tabelist välja, sest agregaatfunktsiooni tulemus on
-- alati ainult üks number ja seda ei saa kuidagi näidata koos väljadega, mida võib
-- olla mitu rida.

-- Keskmise leidmine. Jäetakse välja read, kus height on NULL
SELECT
AVG(height)
FROM
student

-- Palju on üks õpilane keskmiselt laenu võtnud
-- Arvestatakse ka neid õpilasi, kes ei ole laenu võtnud (amount on NULL)
SELECT
AVG (COALESCE(loan.amount, 0))
FROM
student
LEFT JOIN
loan
ON
student.id = loan.student_id;

-- Keskmise leidmine, ümardamine, ridade arvu leidmine jne.
SELECT
ROUND(AVG (COALESCE(loan.amount, 0)), 0) AS "Keskmine laenusumma",
MIN(loan.amount)AS "Minimaalne laenusumma",
MAX(loan.amount)AS "Maksimaalne laenusumma",
COUNT(*) AS "Kõikide ridade arv",
COUNT(loan.amount) AS "Laenude summa" -- Jäetakse välja read, kus loan.amount on NULL
COUNT(student.height) AS "Mitmel õpilasel on pikkus"
FROM
student
LEFT JOIN
loan
ON
student.id = loan.student_id;

-- Kasutades GROUP BY jäävad select päringu jaoks alles vaid need väljad, mis on
-- GROUP BY's ära toodud (antud juhul s.first_name, s.lsdt_name)
-- Teisi välju saab ainult kasutada agregaatfunktsioonide (COUNT jne) sees
SELECT
s.first_name, 
s.lsdt_name, 
SUM(l.amount),
MIN(l.amount),
MAX(l.amount),
AVG(l.amount)
FROM
student AS s
JOIN
loan AS l
ON s.id = l.student_id
GROUP BY
s.first_name, s.lsdt_name

-- Anna mulle laenude summad sünniaastate järgi
-- Anna mulle laenude, mis ületavad 1000, summad sünniaastate järgi
SELECT
date_part('year', s.birthday), SUM(l.amount)
FROM
student AS s
JOIN
loan AS l
ON s.id = l.student_id
GROUP BY
date_part('year', s.birthday)
-- HAVING on nagu WHERE, aga peale GROUP BY kasutamist
-- filtreerimisel saab kasutada ainult neid välju, mis on 
-- GROUP BY's ja agregaatfunktsioone
HAVING
date_part('year', s.birthday) IS NOT NULL
AND SUM(l.amount) > 1000 -- anna ainult need read, kus summa on suurem kui 1000
AND COUNT(l.amount) = 2 -- anna ainult need read, kus laene kokku oli 2

SELECT
s.first_name, s.lsdt_name, date_part('year', l.start_date), SUM(l.amount)
FROM
student AS s
JOIN
loan AS l
ON s.id = l.student_id
GROUP BY
s.first_name, s.lsdt_name, date_part('year', l.start_date)
-- HAVING on nagu WHERE, aga peale GROUP BY kasutamist
-- filtreerimisel saab kasutada ainult neid välju, mis on 
-- GROUP BY's ja agregaatfunktsioone
HAVING
date_part('year', l.start_date) IS NOT NULL
ORDER BY
date_part('year', l.start_date)

-- Anna mulle laenude summad laenutüüpide järgi
SELECT
lt.name, SUM(l.amount)
FROM
loan AS l
JOIN
loan_type AS lt
ON lt.id = l.loan_type_id
GROUP BY
lt.name

-- Anna mulle laenude summad grupeerituna õpilase ning laenutüübi kaupa
-- Tekita mingile õpilasele kaks sama tüüpi laenu
SELECT
s.first_name, s.lsdt_name, lt.name, SUM(l.amount)
FROM
student AS s
JOIN
loan AS l
ON s.id = l.student_id
JOIN
loan_type AS lt
ON lt.id = l.loan_type_id
GROUP BY
s.first_name, s.lsdt_name, lt.name

-- Anna mulle mitu laenu mingist tüübist on võetud ning mis on nende summad
-- sms laen   2     1000
-- kodulaen   1     2300 jne
SELECT
lt.name, COALESCE(SUM(l.amount), 0), COUNT(l.amount) AS "Laenude summa"
FROM
loan_type AS lt
LEFT JOIN
loan AS l
ON lt.id = l.loan_type_id
GROUP BY
lt.name

-- Mis aastal sündinud võtsid suurema summa laene?
SELECT
date_part('year', s.birthday), SUM(l.amount)
FROM
student AS s
JOIN
loan AS l
ON s.id = l.student_id
GROUP BY
date_part('year', s.birthday)
ORDER BY
SUM(l.amount) DESC
LIMIT 2 -- ütleb, mitu rida tulemusest anda

-- Anna mulle õpilaste eesnime esitähe esinemise statistika
-- ehk mitme õpilase eesnimi mingi tähega algab
SELECT SUBSTRING
(s.first_name, 1, 1), COUNT(SUBSTRING(s.first_name, 1, 1))
FROM
student AS s
GROUP BY 
SUBSTRING(s.first_name, 1, 1)

-- Sarnane printf meetodile javas
SELECT FORMAT('Hello %s %s', 233, 233.32)

-- Sarnane indexOf meetodile javas
SELECT POSITION(' ' in 'Sten Aavel')

-- Esimese sõna leidmine
SELECT SUBSTRING('Sten Aavel', 1, POSITION(' ' in 'Sten Aavel'))

-- Viimase sõna leidmine
SELECT SUBSTRING('Sten Aavel', POSITION(' ' in 'Sten Aavel') + 1)

-- Subquery / Nested query / Inner query
-- Anna mulle õpilased, kelle pikkus vastab keskmisele õpilaste pikkusele
SELECT 
first_name, lsdt_name
FROM
student
WHERE
height = (SELECT ROUND(AVG(height)) FROM student WHERE first_name IN ('Johannes')

-- Anna mulle õpilased, kelle eesnimi on keskmise pikkusega õpilaste keskmine nimi
SELECT
first_name, lsdt_name
FROM
student
WHERE
first_name IN
(SELECT 
middle_name
FROM
student
WHERE
height = (SELECT ROUND(AVG(height)) FROM student))

-- Lisa kaks vanimat õpilast töötajate tabelisse
INSERT INTO employee (first_name, lsdt_name, birthday, middle_name)
SELECT first_name, lsdt_name, birthday, middle_name FROM student ORDER BY birthday DESC LIMIT 2

-- Anna mulle kõik matemaatikat õppivad õpilased
SELECT
s.first_name
FROM
student AS s
JOIN
student_subject AS ss
ON s.id = ss.student_id
JOIN
subject AS sub
ON
sub.id = ss.subject_id
WHERE
sub.name = 'Matemaatika'

-- Anna mulle kõik õppeained, kus Mari õpib
SELECT
sub.name
FROM
student AS s
JOIN
student_subject AS ss
ON s.id = ss.student_id
JOIN
subject AS sub
ON sub.id = ss.subject_id
WHERE
s.first_name = "Mari"

-- MySQL-is tähendab schema databaasi

-- Teisendamine täisarvuks
SELECT CAST(ROUND(AVG(age)) AS UNSIGNED) FROM test.emp99;
