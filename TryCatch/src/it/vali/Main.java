package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int a = 0;
        try {
            // Esimene asi, mis vea annab, hüppab talle vastavasse catch plokki (antud juhul int rida esimesena)
            int b = 4 / a;
            String word = null;
            word.length();
            // Exception on klass, millest kõik erinevad Exceptioni tüübid pärinevad,
            // mis omakorda tähendab, et püüdes kinni üldise exceptioni,
            // püüame me kinni kõik exceptionid
        }
        // Kui on mitu catch plokki, siis otsib ta esimese ploki, mis oskab antud exceptioni kinni püüda
        catch (ArithmeticException e) {
            if(e.getMessage().equals("/ by zero")) {
                System.out.println("Nulli ei saa jagada");
            }
            else {
                System.out.println("Aritmeetiline viga");
            }
        }
        catch (RuntimeException e) {
            System.out.println("Reaalajas esines viga");
        }
        catch (Exception e) {
            System.out.println("Esines viga");
        }

        // Küsime kasutajalt numbri ja kui number ei ole korrektne, siis ütleme mingi veateate
        Scanner scanner = new Scanner(System.in);
        boolean correctNumber = false;
        do {
            System.out.println("Nimeta üks number");
            try {
                int answer;
                answer = Integer.parseInt(scanner.nextLine());
                correctNumber = true;
            } catch (NumberFormatException e) {
                System.out.println("Esines viga");
            }
        } while (!correctNumber);

        // Loo täisarvude massiiv 5 täisarvuga ning ürita sinna lisada kuues täisarv
        // Näita veateadet
        int[] numbers = new int[] {1, 2, 3, 4, 5};
        try {
            numbers[5] = 6;
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Indeksit, kuhu tahtsid väärtust määrata, ei eksisteeri");
        }
        catch (Exception e) {
            System.out.println("Esines mingi tundmatu viga");
        }

    }

    }
