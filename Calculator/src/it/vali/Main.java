package it.vali;

public class Main {

    public static void main(String[] args) {
        // Muutuja ja meetodi nimeks mõlemal sum, aga need ei sega üksteist
       int sum = sum(4, 5);
       System.out.printf("Arvude 4 ja 5 summa on %d", sum);
       System.out.println();
       int ans = subtract(6, 2);
       System.out.printf("Arvude 6 ja 2 lahutis on %d", ans);
       System.out.println();
       int fin = multiply(4, 4);
       System.out.printf("Arvude 4 ja 4 korrutis on %d", fin);
       System.out.println();
//       int nul = divide(13, 6);
//       System.out.printf("Arvude 12 ja 6 jagatis on %d", nul);
//        int[] numbers = new int[3];
//        numbers[0] = 1;
//        numbers[1] = 2;
//        numbers[2] = -2;

//        sum = sum(numbers);
//        System.out.printf("Massiivi arvude summa on %d%n", sum);
//// see osa nõuab parandamist
//        numbers = new int[] {2, 5, 12, -12};
//        sum = sum(new int[] {2, 3, 12, 323, 43, 43, -11});
//        System.out.printf("Massiivi arvude summa on %d%n", sum);
//
//        sum = sum(new int[] {2, 3});
//        int[] reversed = som(numbers);
//
//        printNumbers(reversed);
//
//        printNumbers(new int[] {1, 2, 3, 4, 5});
//
//        printNumbers(som(new int[] {1, 2, 3, 4, 5}));
//
//        printNumbers(convertToIntArray(new String[] { "2", "-12", "1", "0", "17" }));

        spacePrint(new String[] {"Tere", "tere", "vana", "kere"});

        facePrint(new String ("... "), new String[] {"Tere", "tere", "vana", "kere"});

        // numbrite keskmine
        System.out.println(countMiddle(new double[] {4, 2, 5, 1}));
//  esimene arv teisest %
        System.out.print(countPercent(5, 10)+"%\n");
// ringi ümbermõõt
        System.out.print(countRing(14));

    }
    // Meetod, mis liidab 2 arvu kokku ja tagastab nende summa
    static int sum(int a, int b) {
        int sum = a + b;
        return sum;
    }
    // Lahuta (subtract), korruta (multiply) ja jaga (divide)
    static int subtract(int c, int d) {
        return c-d;
    }
    static int multiply(int e, int f) {
        return e*f;
    }
    static double divide(int g, int h) {
        return (double)g/h;
    }
    // Meetod, mis võtab parameetrite täisarvude massiivi ja liidab elemendid kokku ning tagastab summa
    static int sum(int[] numbers) {
        int sum = 0;

        for (int i = 0; i < numbers.length; i++) {
            sum += numbers[i];
        }

        return sum;
    }

    // Meetod, mis võtab parameetriks täisarvude massiivi, pöörab tagurpidi ja tagastab selle
    // 1 2 3 4 5
    // 5 4 3 2 1
    static int[] som(int[] numbers) {
        int[] newNumbers = new int[numbers.length];
        for (int i = 0; i < numbers.length; i--) {
            newNumbers[i] = numbers[numbers.length - i - 1];
        }
        return newNumbers;
}
     // Meetod, mis prindib välja täisarvude massiivi elemendid
    static void printNumbers(int[] numbers) {
        for (int i = 0; i < numbers.length; i++) {
            System.out.print(numbers[i]);
        }
    }

    // Meetod, mis võtab parameetriks stringi massiivi (eeldusel, et tegelikult
    // seal massiivis on numbrid stringidena)
    static int[] convertToIntArray(String[] numbersAsText) {
        int[] numbers = new int[numbersAsText.length];

        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = Integer.parseInt(numbersAsText[i]);
        }
        return numbers;
    }

    // Meetod, mis võtab parameetriks stringi massiivi ning tagastab lause, kus iga stringi vahel on tühik (kodutöö)
    static void spacePrint(String[] sentence) {
        String newSentence = new String();
        for (int i = 0; i < sentence.length; i++) {
            newSentence = String.join(" ", sentence);
        }
        System.out.println(newSentence);
    }

    // Eelmine, aga lisaks stringi massiivile võtab teise parameetri, mis on sõnade eraldaja. Tagastab lause. (kodutöö)
    // Vaata eelmist ülesannet, lihtsalt tühiku asemel saad ise valida mingi märgi, mis sõnu eraldab

    static void facePrint(String divider, String[] sentence) {
        String newSentence = new String();
        for (int i = 0; i < sentence.length; i++) {
            newSentence = String.join(divider, sentence);
        }
        System.out.println(newSentence);
    }

    // Meetod, mis leiab numbrite masiivist keskmise ning tagastab selle (liidad numbrid kokku ja siis jagad) (kodutöö)
    // Tagastab double-i

    static double countMiddle(double[] numbers){
        double middle = 0;
        for (int i = 0; i < numbers.length; i++) {
            middle = middle + numbers[i];
        }
        middle = middle/numbers.length;
        return middle;
        // return sum(numbers)/numbers.length peale for-tsüklit on kõige lihtsam lahendus
    }

    // Meetod, mis arvutab, mitu protsenti moodustab esimene arv teisest (kodutöö)
    static double countPercent(double x, double y) {
        double percent = x/y*100;
        return percent;
    }

    // Meetod, mis leiab ringi ümbermõõdu raadiuse järgi
    static double countRing(double r) {
        double round = 2*3.14*r;
        return round;
    }




}
