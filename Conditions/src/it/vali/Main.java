package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("Sisesta number");

        Scanner scanner = new Scanner(System.in);
        int a = Integer.parseInt(scanner.nextLine());
        // Type-safe language (Nt. a-le ei saa anda korraga mitut väärtust, nagu saab JavaScriptis, Pythonis jne.)
        if(a==3) {
            System.out.printf("Arv %d on võrdne kolmega%n", a);
        }
        if(a<5) {
            System.out.printf("Arv %d on väiksem viiest%n", a);
        }
        if(!(a==4)) {
            System.out.printf("Arv %d ei võrdu neljaga%n", a);
        }
        if(a>2) {
            System.out.printf("Arv %d on suurem kahest%n", a);
        }
        if(a>=7) {
            System.out.printf("Arv %d on suurem või võrdne seitsmest%n", a);
        }
        if(!(a>=7)) {
            System.out.printf("Arv %d ei ole suurem või võrdne seitsmega%n", a);
        }
        // arv on 2 ja 8 vahel
        if(a>2 && a<8) {
            System.out.printf("Arv %d on 2 ja 8 vahel%n", a);
        }
        // arv on väiksem kui 2 või arv on suurem kui 8
        if(a<2 || a>8) {
            System.out.printf("Arv %d on väiksem kui 2 või suurem kui 8.%n", a);
        }
        // arv on 2 ja 8 vahel või 5 ja 8 vahel või suurem kui 10
        if((a>2 && a<8) || (a>5 && a<8) || a>10 ) {
            System.out.printf("Arv %d on 2 ja 8 vahel või 5 ja 8 vahel või suurem kui 10%n", a);
        }
        // Kui arv ei ole 4 ja 6 vahel aga on 5 ja 8 vahel või arv on negatiivne aga pole suurem kui -14
        if ((!(a>4 && a<6) && (a>5 && a<8)) || (a<0 && !(a>=-14))) {
            System.out.printf("Arv %d ei ole 4 ja 6 vahel aga on 5 ja 8 vahel või arv on negatiivne aga pole suurem kui -14.%n", a);
        }
        // Kui esimene && tingimus on vale, siis teisi enam ei kontrollitagi
        // Kui esimene || tingimus on õige, siis teisi enam ei kontrollitagi
    }
}
