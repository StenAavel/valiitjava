package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	String sentence = "Väljas on ilus ilm, vihma ei saja ja päike paistab";
	// Split tükeldab Stringi ette antud sümbolite kohalt ja tekitab sõnade massiivi
    // | | tähendab regulaaravaldises või
	String[] words = sentence.split(" ja | |, ");

        for (int i = 0; i < words.length; i++) {
            System.out.println(words[i]);
        }
        System.out.println();

       String newSentence =  String.join(" ", words);

        System.out.println(newSentence);

        System.out.println();

        newSentence =  String.join("\n", words);

        System.out.println();

        System.out.println(newSentence);

        // Escaping (\n), add backspace (\b)
        System.out.println("Juku\b\b\b ütles:\\n \"Mulle meeldib suvi\"");
        // Kui ei taha, et kaldkriips teeks escape, tuleb kasutada topelt kaldkriipsu
        System.out.println("C:\\Users\\opilane\\Documents\\GitHub");

        // Küsi kasutajalt rida numbreid nii, et ta paneb need numbrid kirja ühele reale,
        // eraldades tühikuga. Seejärel liidab need numbrid kokku ja prindib vastuse

        System.out.println("Sisesta arvud, mida tahad omavahel liita, eraldades need tühikuga");
        Scanner scanner = new Scanner(System.in);
        String answer = scanner.nextLine();
        String[] numbers = answer.split(" ");

        int sum = 0;
        for (int i = 0; i < numbers.length; i++) {
            sum += Integer.parseInt(numbers[i]);
        }
        String joinedNumbers = String.join(", ", numbers);
        System.out.printf("Arvude %s summa on %d%n", joinedNumbers, sum);




    }
}
